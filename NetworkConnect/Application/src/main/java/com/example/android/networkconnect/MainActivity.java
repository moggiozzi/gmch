/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.networkconnect;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;

import com.example.android.common.logger.Log;
import com.example.android.common.logger.LogFragment;
import com.example.android.common.logger.LogWrapper;
import com.example.android.common.logger.MessageOnlyLogFilter;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * Sample application demonstrating how to connect to the network and fetch raw
 * HTML. It uses AsyncTask to do the fetch on a background thread. To establish
 * the network connection, it uses HttpURLConnection.
 *
 * This sample uses the logging framework to display log output in the log
 * fragment (LogFragment).
 */
public class MainActivity extends FragmentActivity {

    public static final String TAG = "Network Connect";

    // Reference to the fragment showing events, so we can clear it with a button
    // as necessary.
    private LogFragment mLogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sample_main);

        // Initialize text fragment that displays intro text.
        SimpleTextFragment introFragment = (SimpleTextFragment)
                    getSupportFragmentManager().findFragmentById(R.id.intro_fragment);
        introFragment.setText(R.string.welcome_message);
        introFragment.getTextView().setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0f);

        // Initialize the logging framework.
        initializeLogging();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // When the user clicks FETCH, fetch the first 500 characters of
            // raw HTML from www.google.com.
            case R.id.fetch_action:
                new DownloadTask().execute("http://www.google.com");
                return true;
            // Clear the log view fragment.
            case R.id.clear_action:
              mLogFragment.getLogView().setText("");
              return true;
        }
        return false;
    }

    /**
     * Implementation of AsyncTask, to fetch the data in the background away from
     * the UI thread.
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return loadFromNetwork(urls[0]);
            } catch (IOException e) {
              return getString(R.string.connection_error);
            }
        }

        /**
         * Uses the logging framework to display the output of the fetch
         * operation in the log fragment.
         */
        @Override
        protected void onPostExecute(String result) {
          Log.i(TAG, result);
        }
    }

    private void testGet_ok(){
        try {
            URL url = new URL("http://www.gmch.ru/user");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Start the query
            conn.connect();
            InputStream mstream = new BufferedInputStream(conn.getInputStream());
            Reader reader = null;
            reader = new InputStreamReader(mstream, "UTF-8");
            char[] buffer = new char[4096];
            int cnt = 0;
            StringBuilder sb = new StringBuilder();
            int len = 0;
            while ((len = reader.read(buffer)) > 0) {
                cnt++;
                sb.append(buffer, 0, len);
            }
            String page_str = sb.toString();
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
    }
    private void testPost3(){
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL("http://www.gmch.ru/User/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            //conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Cookie", "_ym_uid=1484409153932115717; _ym_isad=2; UserGuid=61277923");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/");
            conn.setRequestProperty("Accept-Encoding","gzip, deflate, lzma");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            //conn.
            String urlParameters = "__EVENTTARGET=ctl00%24BodyContener%24BtnLogIn&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUJNTc2MTIxMzY5DxYCHhNWYWxpZGF0ZVJlcXVlc3RNb2RlAgEWAmYPZBYCAgMPZBYEAgEPZBYCZg8WAh4EVGV4dAXFEDx1bCBjbGFzcz1NYWluTWVudT48bGkgY2xhc3M9TWFpbk1lbnVfRGVBY3Rpdj48YSBocmVmPSIuLi9oaWVyYXJoeS5hc3B4P2lkPTEiPtCeINC60L7QvNC%2F0LDQvdC40Lg8L2E%2BPC9saT48bGkgY2xhc3M9TWFpbk1lbnVfRGVBY3Rpdj48YSBocmVmPSIuLi9pbmZvLmFzcHg%2FdHlwZT1uZXdzIj7Qn9GA0LXRgdGBIC0g0YbQtdC90YLRgDwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg%2FaWQ9MzAiPtCY0L3RhNC%2B0YDQvNCw0YbQuNGPICDQtNC70Y8gINC%2F0L7RgtGA0LXQsdC40YLQtdC70LXQuTwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg%2FaWQ9MzQiPtCa0L7QvdGC0LDQutGC0L3QsNGPINC40L3RhNC%2B0YDQvNCw0YbQuNGPPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vZmVhZGJhY2suYXNweCI%2B0J7QsdGA0LDRgtC90LDRjyDRgdCy0Y%2FQt9GMPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD05OSI%2B0KTQvtGA0LzRiyDQuCDRgdC%2F0L7RgdC%2B0LHRiyDQvtC%2F0LvQsNGC0YsgPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMDciPtCX0LDQutGD0L%2FQutC4INC4INGC0LXQvdC00LXRgNGLPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMTQiPtCQ0LHQvtC90LXQvdGC0YHQutCw0Y8g0YHQu9GD0LbQsdCwPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMjQiPtCa0L7QvdC60YPRgNGBICLQpNCw0LrQtdC7INGB0L7RgtGA0YPQtNC90LjRh9C10YHRgtCy0LAiPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMjUiPtCf0L7Qu9C40YLQuNC60LAg0L7QsdGA0LDQsdC%2B0YLQutC4INC%2F0LXRgNGB0L7QvdCw0LvRjNC90YvRhSDQtNCw0L3QvdGL0YUg0LIg0J7QntCeICLQk9Cw0LfQv9GA0L7QvCDQvNC10LbRgNC10LPQuNC%2B0L3Qs9Cw0Lcg0KfQtdCx0L7QutGB0LDRgNGLIjwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg%2FaWQ9MTEyNSI%2B0KHQstC10LTQtdC90LjRjyDQviDQt9Cw0LTQvtC70LbQtdC90L3QvtGB0YLQuCDQv9C%2B0YLRgNC10LHQuNGC0LXQu9C10Lkg0L%2FRgNC40YDQvtC00L3QvtCz0L4g0LPQsNC30LAgKNGO0YDQuNC00LjRh9C10YHQutC40YUg0LvQuNGGKTwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg%2FaWQ9MjEyNSI%2B0JrQvtC90LrRg9GA0YHRiyDQuCDQsNGD0LrRhtC40L7QvdGLPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0yMTM0Ij7QodC%2F0L7RgdC%2B0LHRiyDQv9C10YDQtdC00LDRh9C4INC%2F0L7QutCw0LfQsNC90LjQuSDQv9GA0LjQsdC%2B0YDQvtCyINGD0YfQtdGC0LAg0LPQsNC30LAgPC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0zMTM0Ij7QodCy0LXQtNC10L3QuNGPINC%2B0LEg0L7QsdGK0LXQutGC0LDRhSDQvdC10LTQstC40LbQuNC80L7Qs9C%2BINC40LzRg9GJ0LXRgdGC0LLQsDwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg%2FaWQ9NTEzOSI%2B0J%2FQvtC70LjRgtC40LrQsCDQsiDQvtCx0LvQsNGB0YLQuCDQvtGF0YDQsNC90Ysg0YLRgNGD0LTQsCDQuCDQv9GA0L7QvNGL0YjQu9C10L3QvdC%2B0Lkg0LHQtdC30L7Qv9Cw0YHQvdC%2B0YHRgtC4PC9hPjwvbGk%2BPGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY%2BPGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD01MTQ1Ij7QmtC%2B0LzQuNGB0YHQuNGPINC%2F0L4g0LrQvtGA0L%2FQvtGA0LDRgtC40LLQvdC%2B0Lkg0Y3RgtC40LrQtTwvYT48L2xpPjwvdWw%2BZAIDD2QWAgIBD2QWDAIBDw8WAh4HVmlzaWJsZWhkZAIDD2QWAgIBDxYCHwEFCDYxMjc3OTIzZAIFDxYCHwEFQtCf0YDQuNC10Lwg0L%2FQvtC60LDQt9Cw0L3QuNC5INC%2F0YDQuNCx0L7RgNC%2B0LIg0YPRh9C10YLQsCDQs9Cw0LfQsGQCBw8PFgIfAmhkZAIJD2QWCgIBDxYCHwEFwgQ8cD7QlNC70Y8g0L7QsdC10YHQv9C10YfQtdC90LjRjyDQutC%2B0L3RhNC40LTQtdC90YbQuNCw0LvRjNC90L7RgdGC0Lgg0LTQsNC90L3Ri9GFLCDQtNC%2B0L%2FQvtC70L3QuNGC0LXQu9GM0L3QviDQvdGD0LbQvdC%2BINCy0YvQsdGA0LDRgtGMINC00L7QvCwg0LrQvtGA0L%2FRg9GBICjQv9GA0Lgg0L3QsNC70LjRh9C40LgpLCDQvdC%2B0LzQtdGAINC60LLQsNGA0YLQuNGA0YsgKNC%2F0YDQuCDQvdCw0LvQuNGH0LjQuCksICDQv9C%2B0YHQu9C1INGH0LXQs9C%2BINC%2F0L7QtNGC0LLQtdGA0LTQuNGC0Ywg0LDQtNGA0LXRgS48L3A%2BPGRpdiBjbGFzcz0iSW5wdXRMaW5lIj48ZGl2IGNsYXNzPSJGaWxkQ2FwdGlvbkxlZnQiPtCd0LDRgdC10LvQtdC90L3Ri9C5INC%2F0YPQvdC60YI6PC9kaXY%2BPGRpdiBjbGFzcz0iRmlsZElucHV0Ij4g0LMg0KfQtdCx0L7QutGB0LDRgNGLPC9kaXY%2BPC9kaXY%2BPGRpdiBjbGFzcz0iSW5wdXRMaW5lIj48ZGl2IGNsYXNzPSJGaWxkQ2FwdGlvbkxlZnQiPtCj0LvQuNGG0LA6PC9kaXY%2BPGRpdiBjbGFzcz0iRmlsZElucHV0Ij4g0YPQuyDQndC%2B0LLQvtCz0L7RgNC%2B0LTRgdC60LDRjzwvZGl2PjwvZGl2PmQCAw9kFgJmDw8WBB4IQ3NzQ2xhc3MFCUlucHV0TGluZR4EXyFTQgICZBYEAgEPDxYEHwMFD0ZpbGRDYXB0aW9uTGVmdB8EAgJkFgJmDxYCHwEFFtCS0LLQtdC00LjRgtC1INC00L7QvDpkAgMPZBYCZg8QZBAVDhXQstGL0LHRgNCw0YLRjCDQtNC%2B0LwBMAIxNgIxOAIxOQIyMAQyMS81AjI0AjI4AjMwAjM0AjM2AjM4AjQwFQ4AATACMTYCMTgCMTkCMjACMjECMjQCMjgCMzACMzQCMzYCMzgCNDAUKwMOZ2dnZ2dnZ2dnZ2dnZ2dkZAIFD2QWAmYPDxYEHwMFCUlucHV0TGluZR8EAgJkFgQCAQ8PFgQfAwUPRmlsZENhcHRpb25MZWZ0HwQCAmQWAmYPFgIfAQUc0KPQutCw0LbQuNGC0LUg0LrQvtGA0L%2FRg9GBOmQCAw9kFgJmDxBkDxYBZhYBEAUX0J3QtdGCINC30L3QsNGH0LXQvdC40LkFATBnZGQCBw9kFgJmDw8WBB8DBQlJbnB1dExpbmUfBAICZBYEAgEPDxYEHwMFD0ZpbGRDYXB0aW9uTGVmdB8EAgJkFgJmDxYCHwEFINCj0LrQsNC20LjRgtC1INC60LLQsNGA0YLQuNGA0YM6ZAIDD2QWAmYPEGQPFgFmFgEQBRfQndC10YIg0LfQvdCw0YfQtdC90LjQuQUBMGdkZAIJD2QWAmYPDxYEHwMFCUlucHV0TGluZR8EAgJkFgQCAQ8PFgQfAwUPRmlsZENhcHRpb25MZWZ0HwQCAmQWAmYPFgIfAQUe0KPQutCw0LbQuNGC0LUg0LrQvtC80L3QsNGC0YM6ZAIDD2QWAmYPEGQPFgFmFgEQBRfQndC10YIg0LfQvdCw0YfQtdC90LjQuQUBMGdkZAINDw8WAh8CaGQWAgIFDxYCHwEF8wU8cCBjbGFzcz0iTXNvTm9ybWFsIiBzdHlsZT0ibWFyZ2luOiA0Ljg1cHQgMGNtOyB0ZXh0LWFsaWduOiBqdXN0aWZ5OyI%2BPHN0cm9uZz48c3BhbiBzdHlsZT0iZm9udC1zaXplOiAxNC4wcHQ7IGZvbnQtZmFtaWx5OiAmcXVvdDtBcmlhbCZxdW90OywmcXVvdDtzYW5zLXNlcmlmJnF1b3Q7OyBtc28tZmFyZWFzdC1mb250LWZhbWlseTogJnF1b3Q7VGltZXMgTmV3IFJvbWFuJnF1b3Q7OyBjb2xvcjogIzQ4NTA1NjsgbXNvLWZhcmVhc3QtbGFuZ3VhZ2U6IFJVOyI%2B0JIg0LTQsNC90L3QvtC8INGA0LDQt9C00LXQu9C1INCw0LHQvtC90LXQvdGC0YsgLSDQv9C%2B0YHQtdGC0LjRgtC10LvQuCDRgdCw0LnRgtCwINC40LzQtdGO0YIg0LLQvtC30LzQvtC20L3QvtGB0YLRjCDRg9C30L3QsNGC0Ywg0LfQsNC00L7Qu9C20LXQvdC90L7RgdGC0Ywg0LfQsCDQs9Cw0LfQvtGB0L3QsNCx0LbQtdC90LjQtSDQvdCwINC90LDRh9Cw0LvQviDRgtC10LrRg9GJ0LXQs9C%2BINGA0LDRgdGH0LXRgtC90L7Qs9C%2BINC80LXRgdGP0YbQsCwg0L%2FRgNC%2B0YHQvNC%2B0YLRgNC10YLRjCAmbmJzcDvQv9C%2B0LrQsNC30LDQvdC40Y8g0L%2FRgNC40LHQvtGA0L7QsiDRg9GH0LXRgtCwINCz0LDQt9CwJm5ic3A7KNC60YPQsS7QvC4pJm5ic3A70LfQsCDQv9GA0LXQtNGL0LTRg9GJ0LjQuSDQvNC10YHRj9GGINC4INCy0LLQtdGB0YLQuCDQv9C%2B0LrQsNC30LDQvdC40Y8g0L3QsCDRgtC10LrRg9GJ0YPRjiDQtNCw0YLRgy4mbmJzcDs8L3NwYW4%2BPC9zdHJvbmc%2BPC9wPmQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFGWN0bDAwJEJvZHlDb250ZW5lciRTYXZlTWX%2FUygklx%2Fz9l9qUVJBl%2BjWcX01MLONL%2F5S7f%2FpH%2FPwJQ%3D%3D&__VIEWSTATEGENERATOR=70142F31&ctl00%24BodyContener%24HouseFild%24SelectFild=28&ctl00%24BodyContener%24HouseCaseFild%24SelectFild=&ctl00%24BodyContener%24ApartmentFild%24SelectFild=37&ctl00%24BodyContener%24RoomFild%24SelectFild=&ctl00%24BodyContener%24ResultHouse=28&ctl00%24BodyContener%24ResultHouseCase=&ctl00%24BodyContener%24ResultApartment=37&ctl00%24BodyContener%24ResultRoom=&ctl00%24BodyContener%24SaveMe=on";
            // Send post request
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
            wr.write(urlParameters);
            wr.flush();
            wr.close();
            conn.getOutputStream().close();
            //conn.connect();
            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);
            sb.setLength(0);
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                InputStream mstream = new BufferedInputStream(conn.getInputStream());
                Reader reader = new InputStreamReader(mstream, "UTF-8");
                int len = 0;
                char[] buffer = new char[4096];
                while ((len = reader.read(buffer)) > 0) {
                    sb.append(buffer, 0, len);
                }
            }
            String page_str = sb.toString();
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
    }
    //полчение списка домов GET
    private void testCookies(){
        try {
            String COOKIES_HEADER = "Set-Cookie";
            URL url = new URL("http://www.gmch.ru/User/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            //conn.setDoOutput(true);//only for POST
            //conn.setRequestProperty("Cookie", "_ym_uid=1483984644206767745");
            conn.setRequestProperty("Cookie", "_ym_uid=1484409153932115717; _ym_isad=2; UserGuid=61277923");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/");
            conn.setRequestProperty("Accept-Encoding","gzip, deflate, lzma, sdch");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");

            // Start the query
            conn.connect();

            //*
            InputStream mstream = new BufferedInputStream(conn.getInputStream());
            Reader reader = null;
            reader = new InputStreamReader(mstream, "UTF-8");
            char[] buffer = new char[4096];
            int cnt = 0;
            StringBuilder sb = new StringBuilder();
            int len = 0;
            while ((len = reader.read(buffer)) > 0) {
                cnt++;
                sb.append(buffer, 0, len);
            }
            String page_str = sb.toString();
            System.out.println("Response Code : " + conn.getResponseCode());
            //*/
            CookieManager msCookieManager = new CookieManager();

            Map<String, List<String>> headerFields = conn.getHeaderFields();
            List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);
            if (cookiesHeader != null) {
                for (String cookie : cookiesHeader) {
                    msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                }
            }
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
    }
    //проверка данных GET /Service/AdresValidete_Test.ashx?back=room&house=28&HouseCase=&apartment=37
    private void testGET2(){
        try {
            String COOKIES_HEADER = "Set-Cookie";
            URL url = new URL("http://www.gmch.ru/Service/AdresValidete_Test.ashx?back=room&house=28&HouseCase=&apartment=37");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            //conn.setDoOutput(true);//only for POST
            //conn.setRequestProperty("Cookie", "_ym_uid=1483984644206767745");
            conn.setRequestProperty("Cookie", "_ym_uid=1484409153932115717; _ym_isad=2; UserGuid=61277923");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/");
            conn.setRequestProperty("Accept-Encoding","gzip, deflate, lzma, sdch");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");

            // Start the query
            conn.connect();

            //*
            InputStream mstream = new BufferedInputStream(conn.getInputStream());
            Reader reader = null;
            reader = new InputStreamReader(mstream, "UTF-8");
            char[] buffer = new char[4096];
            int cnt = 0;
            StringBuilder sb = new StringBuilder();
            int len = 0;
            while ((len = reader.read(buffer)) > 0) {
                cnt++;
                sb.append(buffer, 0, len);
            }
            String page_str = sb.toString();
            System.out.println("Response Code : " + conn.getResponseCode());
            //*/
            CookieManager msCookieManager = new CookieManager();

            Map<String, List<String>> headerFields = conn.getHeaderFields();
            List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);
            if (cookiesHeader != null) {
                for (String cookie : cookiesHeader) {
                    msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                }
            }
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
    }
    /** Initiates the fetch operation. */
    private String loadFromNetwork(String urlString) throws IOException {
        InputStream stream = null;
        String str ="";

        try {
            //stream = downloadUrl(urlString);
            //str = readIt(stream, 500);
            //testCookies();
            //testGET2();
            testPost3();
       } finally {
           if (stream != null) {
               stream.close();
            }
        }
        return str;
    }

    /**
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString A string representation of a URL.
     * @return An InputStream retrieved from a successful HttpURLConnection.
     * @throws java.io.IOException
     */
    private InputStream downloadUrl(String urlString) throws IOException {
        // BEGIN_INCLUDE(get_inputstream)
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Start the query
        conn.connect();
        InputStream stream = conn.getInputStream();
        return stream;
        // END_INCLUDE(get_inputstream)
    }

    /** Reads an InputStream and converts it to a String.
     * @param stream InputStream containing HTML from targeted site.
     * @param len Length of string that this method returns.
     * @return String concatenated according to len parameter.
     * @throws java.io.IOException
     * @throws java.io.UnsupportedEncodingException
     */
    private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    /** Create a chain of targets that will receive log data */
    public void initializeLogging() {

        // Using Log, front-end to the logging chain, emulates
        // android.util.log method signatures.

        // Wraps Android's native log framework
        LogWrapper logWrapper = new LogWrapper();
        Log.setLogNode(logWrapper);

        // A filter that strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        mLogFragment =
                (LogFragment) getSupportFragmentManager().findFragmentById(R.id.log_fragment);
        msgFilter.setNext(mLogFragment.getLogView());
    }
}
