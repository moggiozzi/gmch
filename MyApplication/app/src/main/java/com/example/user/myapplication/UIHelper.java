package com.example.user.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by User on 24.01.2017.
 */

public class UIHelper {
    static public void showMessageBox(Activity a, String title, String message){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(a);
        dlgAlert.setTitle(title);
        if(message!=null)
            dlgAlert.setMessage(message);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dismiss the dialog
                    }
                });
        dlgAlert.create().show();
    }
}
