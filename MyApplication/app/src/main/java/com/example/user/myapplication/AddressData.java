package com.example.user.myapplication;

import java.util.ArrayList;

/**
 * Created by User on 21.01.2017.
 */

public class AddressData{
    public String locality;
    public String street;
    public ArrayList<String> houseList;
    public String house;
    public ArrayList<String> corpusList;
    public String corpus;
    public String appartment;
    public AddressData(){
        houseList = new ArrayList<String>();
        corpusList = new ArrayList<String>();
        reset();
    }
    public AddressData(AddressData ad){
        locality = ad.locality;
        street = ad.street;
        houseList = new ArrayList<String>();
        for(int i=0;i<ad.houseList.size();i++)
            houseList.add(ad.houseList.get(i));
        house = ad.house;
        corpusList = new ArrayList<String>();
        for(int i=0;i<ad.corpusList.size();i++)
            corpusList.add(ad.corpusList.get(i));
        corpus = ad.corpus;
        appartment = ad.appartment;
    }
    public void reset(){
        locality=street=house=appartment="";
        houseList.clear();
        corpusList.clear();
    }
}
