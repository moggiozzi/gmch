package com.example.user.myapplication;

import android.app.Activity;
import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.user.myapplication.DownloadTask.DownloadResult.NO_DATA;
import static com.example.user.myapplication.DownloadTask.DownloadResult.NO_PARSE;

/**
 * Created by User on 15.01.2017.
 */

public class DownloadTask extends AsyncTask<Integer, Void, String> {
    // Отладочная переменная. Если true, то реальное подключение к сети и передача данных не выполняется.
    //private static final boolean EMULATE_INTERNET = true;
    private static final boolean EMULATE_INTERNET = false;

    enum DownloadResult{ NO_DATA, NO_PARSE, OK };
    DownloadResult downloadResult = NO_DATA;
    private Activity activity = null;
    private int st = 0;
    public DownloadTask(Activity a){
        activity = a;
    }
    @Override
    protected void onPreExecute() {
        //display progress dialog.
    }
    @Override
    protected String doInBackground(Integer... step) {
        String res = "";
        st = step[0];
        MyApplication app = (MyApplication)activity.getApplication();
        //try {
        downloadResult = DownloadResult.OK;
        if(EMULATE_INTERNET)
            return "";
        switch (step[0]){
            case 1:
                String userId = app.userData.userId;
                String cookie = app.userData.cookie;
                res = InternetHelper.getAddressPage(app.userData.userId,app.userData.cookie);
                if(res.length()>0) {
                    if(parseAddressPage(res, app.userData.addressData)==false)
                        downloadResult = NO_PARSE;
                }else{
                    downloadResult = NO_DATA;
                }
            break;
            case 2:
                res = InternetHelper.getDataPage(app.userData.userId,app.userData.cookie,app.userData.addressData);
                if(res.length()>0) {
                    if(parseDataPage(res, app.userData)==false)
                        downloadResult = NO_PARSE;
                }else{
                    downloadResult = NO_PARSE;
                }
                break;
            case 3:
                res = InternetHelper.updateDataPage(app.userData.userId,app.userData.cookie,app.userData);
                if(res.length()>0) {
                    if(parseDataPage(res,app.userData)==false)
                        downloadResult = NO_PARSE;
                }else{
                    downloadResult = NO_PARSE;
                }
                break;
        }
        //} catch (Exception e) {
            //return getString(R.string.connection_error);
        //}
        return res;
    }
    @Override
    protected void onPostExecute(String result) {
        //update UI
        switch (st){
            case 1:
                ((MainActivity)activity).postDownload(downloadResult);
                break;
            case 2:
                ((AddressActivity)activity).postDownload(downloadResult);
                break;
            case 3:
                ((DataActivity)activity).postDownload(downloadResult);
                break;
        }
    }

    private boolean parseAddressPage(String addressPage, AddressData addressData){
        Document doc = Jsoup.parse(addressPage);
        Elements e2 = doc.select("#BodyContener_Messege > .InputLine > .FildInput");
        addressData.reset();
        if(e2.size()>=6) {
            addressData.locality = e2.get(0).text(); //населенный пункт
            addressData.street = e2.get(1).text(); //улица
            Element houses = e2.get(2).children().first();
            if (houses != null) {
                Elements childs = houses.children();
                for (int i = 0; i < childs.size(); i++) {
                    if (Element.class.isInstance(childs.get(i))) {
                        Element el = childs.get(i);
                        if (el.attr("value") != "")
                            addressData.houseList.add(el.attr("value"));
                    }
                }
            }
            Element corps = e2.get(3).children().first();
            if (corps != null) {
                Elements childs = corps.children();
                for (int i = 0; i < childs.size(); i++) {
                    if (Element.class.isInstance(childs.get(i))) {
                        Element el = childs.get(i);
                        if (el.attr("value") != "")
                            addressData.corpusList.add(el.attr("value"));
                    }
                }
            }

        }
        if(addressData.locality.length()>0 &&
            addressData.street.length()>0 &&
            addressData.houseList.size()>0)
        {
            return true;
        }
        return false;
    }
    private boolean parseDataPage(String dataPage, UserData userData){
        Document doc = Jsoup.parse(dataPage);
        Elements e1 = doc.select(".Saldo"); // Переплата
        Elements e2 = doc.select("#BodyContener_Counter1_Block > .CountersTitle"); // заводской номер
        Elements e3 = doc.select("#BodyContener_Counter1Info"); //предыдущее показание
        Elements e4 = doc.select("#BodyContener_Counter1NewInfo"); // текущее показание
        Elements e5 = doc.select("#BodyContener_Counter1_Raznica"); // потребление
        Elements e6 = doc.select("#BodyContener_Counter1_Block"); // последний элемент: Дата изменения
        userData.reset();
        userData.saldo = e1.text();
        userData.factoryNumber = e2.text();
        userData.lastCounter = e3.text();
        userData.currentCounter = e4.attr("value");
        userData.consumeCounter = e5.text();
        if(e6.size()>0 && e6.get(0).children().size()>4){
            userData.date = e6.get(0).children().get(4).text();
        }
        if(userData.saldo.length()>0&&
            userData.factoryNumber.length()>0)
        {
            return true;
        }
        return false;
    }
}

