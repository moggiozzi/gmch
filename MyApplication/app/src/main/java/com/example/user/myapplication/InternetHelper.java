package com.example.user.myapplication;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PushbackInputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * Created by User on 15.01.2017.
 */

public class InternetHelper {
    static public boolean checkNetworkConnection(Activity a) {
        ConnectivityManager connMgr = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected())
            return true;
        return false;
    }
    static public String getAddressPage(String userId, String cookie){
        String page_str="";
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL("http://www.gmch.ru/User/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Cookie", "_ym_uid="+cookie+"; _ym_isad=2; UserGuid="+userId);
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/");
            conn.setRequestProperty("Accept-Encoding","gzip");//, deflate, lzma");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");

            String urlParameters = "__VIEWSTATEGENERATOR=70142F31&"+
                    "ctl00%24BodyContener%24AccountNumber="+userId+
                    "&ctl00%24BodyContener%24LogIn=Вход";
            // Send post request
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
            wr.write(urlParameters);
            wr.flush();
            wr.close();
            conn.getOutputStream().close();
            //conn.connect();
            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);
            sb.setLength(0);
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                InputStream mstream = new BufferedInputStream(decompressStream(conn.getInputStream()));
                Reader reader = new InputStreamReader(mstream, "UTF-8");
                int len = 0;
                char[] buffer = new char[4096];
                while ((len = reader.read(buffer)) > 0) {
                    sb.append(buffer, 0, len);
                }
            }
            page_str = sb.toString();
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
        return page_str;
    }
    static public String getDataPage(String userId, String cookie, AddressData addressData) {
        if(sendAddress(userId,cookie,addressData))
            return getDataPage(userId,cookie);
        return "";
    }
    static public String updateDataPage(String userId, String cookie, UserData userData){
        if(sendData(userId,cookie,userData))
            return getDataPage(userId,cookie);
        return "";
    }
    static boolean sendAddress(String userId, String cookie, AddressData addressData){
        String page_str="";
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL("http://www.gmch.ru/User/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Cookie", "_ym_uid="+cookie+"; _ym_isad=2; UserGuid="+userId);
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/");
            conn.setRequestProperty("Accept-Encoding","gzip");//, deflate, lzma");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");

            String urlParameters = "ctl00%24BodyContener%24HouseFild%24SelectFild="+addressData.house+
                    "&ctl00%24BodyContener%24HouseCaseFild%24SelectFild="+addressData.corpus+
                    "&ctl00%24BodyContener%24ApartmentFild%24SelectFild="+addressData.appartment+
                    "&ctl00%24BodyContener%24RoomFild%24SelectFild=&" +
                    "ctl00%24BodyContener%24ResultHouse="+addressData.house+
                    "&ctl00%24BodyContener%24ResultHouseCase="+addressData.corpus+
                    "&ctl00%24BodyContener%24ResultApartment="+addressData.appartment+
                    "&ctl00%24BodyContener%24ResultRoom=&" +
                    "ctl00%24BodyContener%24SaveMe=on";
            // Send post request
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
            wr.write(urlParameters);
            wr.flush();
            wr.close();
            conn.getOutputStream().close();
            //conn.connect();
            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);
            sb.setLength(0);
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                InputStream mstream = new BufferedInputStream(decompressStream(conn.getInputStream()));
                Reader reader = new InputStreamReader(mstream, "UTF-8");
                int len = 0;
                char[] buffer = new char[4096];
                while ((len = reader.read(buffer)) > 0) {
                    sb.append(buffer, 0, len);
                }
            }
            page_str = sb.toString();
            if(responseCode == HttpURLConnection.HTTP_OK)
                return true;
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
        return false;
    }
    static String getDataPage(String userId, String cookie){
        String page_str="";
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL("http://www.gmch.ru/User/default.aspx");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Cookie", "_ym_uid="+cookie+"; _ym_isad=2; UserGuid="+userId+"; Authorization=1");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/");
            conn.setRequestProperty("Accept-Encoding","gzip");//, deflate, lzma");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            conn.connect();
            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            sb.setLength(0);
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                InputStream mstream = new BufferedInputStream(decompressStream(conn.getInputStream()));
                Reader reader = new InputStreamReader(mstream, "UTF-8");
                int len = 0;
                char[] buffer = new char[4096];
                while ((len = reader.read(buffer)) > 0) {
                    sb.append(buffer, 0, len);
                }
            }
            page_str = sb.toString();
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
        return page_str;
    }
    static boolean sendData(String userId, String cookie, UserData userData){
        String page_str="";
        try {
            StringBuilder sb = new StringBuilder();
            URL url = new URL("http://www.gmch.ru/User/default.aspx");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Cookie", "_ym_uid="+cookie+"; UserGuid="+userId+"; Authorization=1; _ym_isad=2");
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.setRequestProperty("Cache-Control","max-age=0");
            conn.setRequestProperty("Upgrade-Insecure-Requests","1");
            conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94");
            conn.setRequestProperty("Referer","http://www.gmch.ru/User/default.aspx");
            conn.setRequestProperty("Accept-Encoding","gzip");//, deflate, lzma");
            conn.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            conn.setRequestProperty("Origin","http://www.gmch.ru");

            String urlParameters =
                    "__EVENTTARGET=&__EVENTARGUMENT=&"+
                    "__VIEWSTATEGENERATOR=70142F31&ctl00%24BodyContener%24Counter1NewInfo="+userData.currentCounter+
                    "&ctl00%24BodyContener%24ChengeInfo=Сохранить";
            // Send post request
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
            wr.write(urlParameters);
            wr.flush();
            wr.close();
            conn.getOutputStream().close();

            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);
            sb.setLength(0);
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                InputStream mstream = new BufferedInputStream(decompressStream(conn.getInputStream()));
                Reader reader = new InputStreamReader(mstream, "UTF-8");
                int len = 0;
                char[] buffer = new char[4096];
                while ((len = reader.read(buffer)) > 0) {
                    sb.append(buffer, 0, len);
                }
            }
            page_str = sb.toString();
            if(responseCode == HttpURLConnection.HTTP_OK)
                return true;
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
        return false;
    }

    public static InputStream decompressStream(InputStream input) {
        PushbackInputStream pb = new PushbackInputStream( input, 2 ); //we need a pushbackstream to look ahead
        byte [] signature = new byte[2];
        try {
            int len = pb.read(signature); //read the signature
            pb.unread(signature, 0, len); //push back the signature to the stream
            if (signature[0] == (byte) 0x1f && signature[1] == (byte) 0x8b) //check if matches standard gzip magic number
                return new GZIPInputStream(pb);
        }catch (Exception ex){
            System.out.print(ex.getMessage());
        }
        return pb;
    }
}
