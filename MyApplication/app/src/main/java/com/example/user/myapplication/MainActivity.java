package com.example.user.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListPopupWindow;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener,
        AdapterView.OnItemClickListener {
    ArrayList<String> list;
    ListPopupWindow lpw;
    EditText editText;
    MyApplication myApp;

    @Override
    protected void onStart() {
        super.onStart();

        list = new ArrayList<String>();
        ArrayList<UserData> udl = myApp.userDataList;
        for(int i=0;i<udl.size();i++)
            list.add(udl.get(i).userId);
        lpw = new ListPopupWindow(this);
        lpw.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, list));
        lpw.setAnchorView(editText);
        lpw.setModal(true);
        lpw.setOnItemClickListener(this);
        if(udl.size()>0)
            lpw.setSelection(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myApp = (MyApplication)getApplication();
        myApp.loadData();

        editText = (EditText) findViewById(R.id.accauntNumber);
        editText.setOnTouchListener(this);
        //System.out.print(((MyApplication)getApplication()).UserId);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        String item = list.get(position);
        editText.setText(item);
        lpw.dismiss();
        myApp.userData =
            myApp.userDataList.get(position);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int DRAWABLE_RIGHT = 2;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getX() >= (v.getWidth() - ((EditText) v)
                    .getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                lpw.show();
                return true;
            }
        }
        return false;
    }

    public void page1Enter(View view)
    {
        if(!InternetHelper.checkNetworkConnection(this)) {
            UIHelper.showMessageBox(this, "Нет подключения к сети.", null);
            return;
        }
        EditText editText = (EditText) findViewById(R.id.accauntNumber);
        if(editText.getText().toString().length() < 4) {
            UIHelper.showMessageBox(this, "Некорректный номер счета!", null);
            return;
        }
        String id = editText.getText().toString();
        // если ввели имя нового пользователя
        if(myApp.getUserDataById(id) == null) {
            UserData ud = new UserData();
            myApp.userData = ud;
        }
        myApp.userData.userId = id;
        new DownloadTask(this).execute(1);
    }

    public void postDownload(DownloadTask.DownloadResult downloadResult){
        switch (downloadResult){
            case OK:{
                Intent intent = new Intent(MainActivity.this, AddressActivity.class);
                startActivity(intent);
            }break;
            case NO_DATA:{
                UIHelper.showMessageBox(this,"Нет связи с сервером.",null);
            }break;
            case NO_PARSE:{
                UIHelper.showMessageBox(this,"Не удалось получить адрес.","Проверьте номер лицевого счета.");
            }break;
        }
    }
    public void test(View view) {
        myApp.clearAllData();
    }
    public void testSaveLoad(View view){
        try {
        //1)
        Context context = getApplicationContext();
//        SharedPreferences sharedPref = context.getSharedPreferences(
//                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences sharedPref = context.getSharedPreferences("info.dat",Context.MODE_PRIVATE);
        //write
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("f1", 11);
        editor.commit();
        //read
        long highScore = sharedPref.getInt("f1", 0);
            //2)
//        File file = getFilesDir();
//        boolean isr = file.canRead();
//        boolean isw = file.canWrite();
//        long l = file.length();
//        boolean isd = file.isDirectory();
//        boolean ex = file.exists();
//        boolean isf = file.isFile();
//            String settingsFileName = "gmch_settings.dat";
//            FileOutputStream fos = openFileOutput(settingsFileName, Context.MODE_PRIVATE);
//            ObjectOutputStream os = new ObjectOutputStream(fos);
//            AddressData ad1 = new AddressData();
//            ad1.corpusList.add("abcd"); ad1.corpusList.add("123");
//
//            ad1.street = "str";
//            AddressData ad2;// = new AddressData();
//            os.writeObject(ad1);
//            os.close();
//            fos.close();
//
//            FileInputStream fis = openFileInput(settingsFileName);
//            ObjectInputStream is = new ObjectInputStream(fis);
//            ad2 = (AddressData) is.readObject();
//            is.close();
//            fis.close();
            //File file = new File(context.getFilesDir(), filename);
            System.out.print("end test\n");
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
    }
    public void testJsoup(View view){
        String testPage = "<!DOCTYPE html>\n" +
                "<!-- saved from url=(0020)http://gmch.ru/User/ -->\n" +
                "<html><head id=\"Head1\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "</head>\n" +
                "<body>\n" +
                "    <div id=\"noscript\" class=\"error\" style=\"display: none;\">\n" +
                "        <img alt=\"\" title=\"\" src=\"./02_Корпусы ООО «Газпром межрегионгаз Чебоксары» Прием показаний приборов учета газа_files/attantion.jpg\">Чтобы вам были доступны все наши сервисы, пожалуйста, включите JavaScript!\n" +
                "    </div>\n" +
                "    <div id=\"Canvas\">\n" +
                "\t\n" +
                "        <div id=\"SeatchBlock\">\n" +
                "            <div id=\"Poisk\">\n" +
                "                <div class=\"SerchIcon\"><img alt=\"\" title=\"\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+EDcmh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBSaWdodHM9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9yaWdodHMvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wUmlnaHRzOk1hcmtlZD0iRmFsc2UiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OEU3RDQxMjk3QjBBMTFFMEE0OUNFRTQzNDRCQzZDNTYiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OEU3RDQxMjg3QjBBMTFFMEE0OUNFRTQzNDRCQzZDNTYiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTMyBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InV1aWQ6RkM3QUJCNEZFQzdBRTAxMTk0MzRBNzVEMEVDNDkwODYiIHN0UmVmOmRvY3VtZW50SUQ9InV1aWQ6OUFGMUQxOUJFMjdBRTAxMTk0MzRBNzVEMEVDNDkwODYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCAARABEDAREAAhEBAxEB/8QAYwAAAgMAAAAAAAAAAAAAAAAAAAYBBwoBAQEBAAAAAAAAAAAAAAAAAAABAhAAAgMAAgMAAwAAAAAAAAAAAgMBBAUAESETBhIiFBEBAQEBAQEBAQAAAAAAAAAAAQARMUEhAiL/2gAMAwEAAhEDEQA/AN5m7vaFXQpYWFQRf2b1d10puOOvn59BBiords1ibj9jjgAAI7Iu/MRHKHzXlFeHaMPe0bGlcwN6jXo7NSqrQUdJx2M/Rz2sJH9NU2gDlkl4/gxZx3EzExMxPCenILuM28lZR3sPUfpUd/5+3Uq69Ks6g1OitrM/Rz3sF01rEoKHoYp4QYGHcxMzEx1PKPjyiO6dlWtT+3zvsqW1qVc7Yr69QcW1OT7kr+erhYmyLBm1Psegj/YymOynxHXQxN+Zln+j9b5WxzNuOJHEjiX/2Q==\"></div>\n" +
                "                <div class=\"SerchText\"><input id=\"TextSearch\" type=\"text\"></div>\n" +
                "                <div class=\"SerchBtn\"><a id=\"BtnSearch\" title=\"\" href=\"http://gmch.ru/search.aspx\">Найти</a></div>\n" +
                "            </div>\n" +
                "            <div id=\"SiteMap\"><a title=\"Карта сайта\" href=\"http://gmch.ru/map.aspx\">Карта сайта</a></div>\n" +
                "        </div>\n" +
                "        <div id=\"Headblock\">\n" +
                "            <div class=\"LeftBlock\"><a title=\"Официальный сайт ООО «Газпром межрегионгаз Чебоксары»\" href=\"http://gmch.ru/\"><img alt=\"логотип ООО «Газпром межрегионгаз Чебоксары»\" title=\"ООО «Газпром межрегионгаз Чебоксары»\" src=\"data:image/gif;base64,R0lGODlhrABdAPcAAAV1x4zA57DU7u71++Xx+pXF6SSGz5nG6bzb8BN9y+32+53K6ni14czj9R2DzazR7AR0xgBxx3S04H+441Sh2Ym+5QBzxkub2Gms3vj8/9bp92Kp3P3+/9zs+bjY7xR+ytjq+CKEzWSq3ejz+k2c2CyK0IO649Hn9Qh3yLTW787k9aTN6z+U1BuBzHCw3jqS0snh9QByw73c8eDu+X234qbN7BF7ySiJznaz4Qp3yfP4/ZfF50ma14G55Ov0+1ij2vf7/l2m22Cn3Nzt95DC5xh/zMHd8h6EzanQ7YW844W745LC5gJyxmut4CuJ0TeQ01Gg2Vuk2sfg9IS75LrY8Ia85KPM6iWG0PT5/W6u3yeIz0OX1XSz3xmAy7bX8C6M0e/2/MLe88ji81Of2ZHD6Pz9/+Pw+er1+9bp+ESY1vD4+3Kx4Onz/ECW06jO6zmS1I6/5kCV1dnp+KDL6zOP0gByxzKO0TaP0/v//xF7zHe04ZjE5xaAzUGX1At4ytTn9hV/zPT5/wF0x0KW1srk9cPf8TiP0wRyxz2S0gx7yv/+//7//////QBxxP7+/gFxxen0+lCf2Nrq9wx6yZvI6dvs9tDl9g96yvX6/dXo9sTf8tjq9jCN0kGW1uLv+DiR08Xg80eZ1f7+/wBwxEeY1w96zKHM7Of0+v7+/P/9/o3B5/7//XW04A55yen0+Pj8/Qd4yNfp9cHe8Nvr+LnX71+o27/b8eHv+tbq9ef0/F+m25DC5QBzw7HV7NHm94G34w58y/b6/fX6/vr7/9Tq97jW8AJ1x9zq9zOO1HS04vv8/szj80+e1+Lu+hB6yESY1DGN0J/M6juT0z+V0tTn+Pz+/a/T7er1+RR6zBR7ylaj29Dm9ESW0lOg2KfQ7NPp9iCDzgNzx7bZ71ij3KjQ6uHw9/X7+2Sr3Wer3MTf9EWW1UCV1s7l9ezz++/5+8Ld8MLd8iuM0DKOz2ir30aX2M/l85HB5eHt+eHt+zmR0TiS0TqS0djo9YO94wByxf///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBSaWdodHM9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9yaWdodHMvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wUmlnaHRzOk1hcmtlZD0iRmFsc2UiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6ODc2QTI5NDg3QUZFMTFFMEE0QkFDRTZGRDY2NkUxNEQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6ODc2QTI5NDc3QUZFMTFFMEE0QkFDRTZGRDY2NkUxNEQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTMyBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InV1aWQ6RkM3QUJCNEZFQzdBRTAxMTk0MzRBNzVEMEVDNDkwODYiIHN0UmVmOmRvY3VtZW50SUQ9InV1aWQ6OUFGMUQxOUJFMjdBRTAxMTk0MzRBNzVEMEVDNDkwODYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQAAAAAACwAAAAArABdAAAI/wD/CRxIsKDBgwj/LRqo6F+ZeA8SSpxIsaLFixgzatxYkdE/Iv7AYULYkKPJkyhTqjzJJs8jCzQMOlpJs6bNmxdn/pu5wZ+/Ryg6pBKoSBEjnTiTKl2qclEDCP5c+PFHomBJplizajXocSAPf0WwMPCpieDVrWjT2jy7SAWTGAH+DbDhj5nau3iXUgALROAUf+E05B1MGCUICBYmDNRRyt8Gs0gLS5ZcsmsWfwAIEHThD4Vms5NDh3ZUpogFCgMXdgDgT4nC1KJjF07hE0FBRiQiPIEtu/fdhj8sdFlYEIk/QWZ8K0/rUZGOHFEHCm6IyU+EAsuzb3VkLYY/KQIXff8xMnDvIO3omS7iDK4kEgtxBj7ADOZ11/T4VX6JkGUgHbKo/KMGVALkZ2BTCkBVg0AN+GRBEAN94Y8JB1Z4kgwR+DODQD350wgKCszURAyoWWhiRiB1IRAQk/jkTwxELLSCP1qcaKNFPUHhEW0u+iPNP4pk4g8EmNx345EFveGPHgJh0OOQZ/yTAVQwIGllQQb4g90/Xbj4yE9WCJTlAcRdieQiefjTyz8q9PilP9oIxMMjipmZHiOiTLQIBDFYoogST9YRgw1l/BNFI/1FNtBRRFll0UJlTtQcRguVZClJB0XKG1NnHTRAI/4o8M8TT/oUgy3/NOEPhLct1JWRXDX/umhBxEVWVEYNNaQpRYruKutSiiY0hD+T/MMGVD6B6lMdXPxDA1WOwkqQrwR5RBy14WF066wHdfpaQmfl6pFHijISoEk6XcvIQhl4kgko9TUIziI8JtvjIKgE0MiPqQEBwr8ABwxpwJv8q0F9tGJRCQgFg0AAB2cpEHDAcvDD8EASUwyCHASs8o+0OiwMMAGatgOCBhsXLGpBZ8hxMsAa5UoQELbQQMIVyPpDHgI0/lPBk40oqyIlj3xRkBR1uBjB0i5m8A8BpfoTQRJAhkcACRBk6GIjRRxAUBLJKvskCgOZIHXULSwgUFeekBKOqS62YApBY23tT38KVapNqScB/1HDBShErfM/mjRyxT+6vBm1DvPZ8eo/UrhoQalON9ij1ngPpKSL3m2dwkBJfCn22GUL7lM4tkWotT8WiB2OETMtMtbkL/oT38cDacH3Rp404YzgWsPzjwZg/XMHqKO72AASEfyIVORu+pRhBo4IIHg3BIEgOKiPUNAV2EqXSrZAZpu+6kBtrl6qEAPV3WMC0wKBLO3+aETABqw9OcqTFpBnhj85+EcRzLeCA/gjFEd70gFSwMAULMSASfPHNFz0hrX9YwFPgsPmfKKimVThfV5ogthgUZLy+eQD5MjS2QxAlAL0KBxLQIZPvlQjgbjPS5/5mAoi2CML/sogZQiAM/86Zz6fkCcQPslA/rxDP59U4Wc/SGCPJAGrCbioFaryyREIEoCz+eMDjDiA2AIokH6IrQX/2EGPxveP8mXIAf/oweryMJC/vImFIHERH9rnoSd5gCDR8KKLKFIpSdihdg4qInkYAYFGaO8npZocA8bSLIJAz0WSmNY/ROAiBzzLJ2z02U8mV4R/UKJHEJgJI6oQgQiiUY0uYmP5kgbHJdirFQP5RY/w2KMP8LFUFSDIGhI5SE0epAatWFYRe0QeRdDFA0+6AiR90gR0+IMIUnTRMQxyARd9oYsu8sFAfuaiIijilC4CAOh69Mo1lq6T/7Cli4olkE/6hJcugp8Noxb/har9Iw09ot20HjcQewoyAk0slREWQgcLmCJ8AiDVDKPADH9YQyA6uaRPMhkZCfkkDhh00QnG2aNSonNy6gQS+HzSzli+80tblKdPEjEQK7oInz5JgE5uqCyj5WqAHlrdD6d1jsuxTnqK5Ik/GCA2AyzCCj0ige6mMxCN+mMWBuGDiyJhjR6JoyQ16IRYOxGJc/Yopf9YqT9aCsp3+qSWiaTnP2x6z4+875dPAoAyBAKJPj5pIhtInuSW2UxVUGWG/ogLEH4nNWkAwA8cyOZGfciBnG1ABj1Smz9lZcAZ2mCdcUujO8lnLzjao0dypas/cOoPff6jbkHr0Qk8QgXB/92GIHpYpiI/xjMVgsoTHtkLB/2xG8n6I5ME+Z+L1mAJL1VhIMH6S+f4lVZ2italpO0kI2RKrJru0q75/KV3jvABn6xAIF18hBYuIbaEqFF9CVVf1MhjrCHddCBLsECGwmEBdHTKqsgdCAxWNwVP9KgJO0HKECyhAms4oEfRGIgSmBBaWLY1u2+NJ2q9e9885lS8EdACC/xRByb9Qwg+oUAeiFgt9AVOt+aj7z+c0KNaEKe5PaqBpgBcra76g8IFUMMj3hSJSg1kC16aXAuIcB+wKYutnXGrP+A6w9R+18OtxSuNrOkPHgiEE5OjQQJ6aBBMSBPGMX4NZ1ykioFgYv91gnAFUbrCY4K40EULmp8hCDITJHvxCTj4A+6qW+HRtrFHcCTCm2JAR4GolrWuvaEW8ohGUby4BmMWqEG4oDg0z5c4xejRecMzZp/EYyfGDXA9j+oPL/xjzF+CY2oWEQVwtCB/PpmEJoijVijLEtGM0IbY5iWQHlz5rvt0kRY8oDUFCMknKii1T3hDDdYI1tNGdPOL/ZE6hXDCRXoIVp1R3ROtgefMmCmTrgQChjv0KA0Y7fV1L3zodD4YkVF09LHDm+wXaWEGLrKFG3wiCEzQRdMEeYZfsa1QgpBAWSe4SihclImuXGXcclIaCBShD6WxoSBIIWcMItDo6j553lH/xrBRl/Y5hVhRa5D+Zeu0sAjr+IMMY3nEERgh7foRBBQMN18YiCOKgfukSq/phE+KaxCM/+MLYrPDJy6xOkso5BVgyDoY1GDKkfsEACWRt4VTXu+o2aDNHK4rliOttBrZ4UsbIIFPvIwNMg+EBJ0Oeo/CQJBgtMgfBXpNCb60paY/SRKR6nkkXS3KPprUH/uLAVqdzFKU/1p6XRCD5kHgNIZMQL9qR/Zre1SjKCwdHENewz/oUsyGSOJtjahD3oMuY4GY3h9eG0gXIgAiRniLx67KgCCA5xPNVmB15qQEESdv3bFfPsOw0skvmgreD/ebRor4C++15jXWT3ttqp39//ZMV/t/GAFUdZISa0rEZ4Fo1AJYDc8tEpkABu5Na1MQCDl98nhIplQRH1RhjfAmbPQX8JQQNvUlLEQGiMV2kFQj8/EkfNdzqXEEPUJERUREGUI/5BEpI+Zl7ucTfyQTjvB+qqZRJaB/Z9MIGKCCkNR/CKVODQEooXVnrHN5oCJrfEYcj/YPDPgm8NMQDPAlGVIjf6A1MfAlUUKBAtEmxGQ6nSN+iiNjR7EIPJMHxGFYd9BiltQI9KNqEegT/NJZUmMXoqQsjwcqFjA+jkCDkIdGe4BIz7dWEqFah0MGq+OAPlEjU4JsTPgPqiB+paIsGOhXKHADnRAFqjYQ0uAPKv8gEN3kAd6SURbARMfFCF2xBMpiAVXxDxE4OXv2DwXwJh/CBqzQI3kQduzECHcGKmRUdlNmTETxM5PTCJOgBlnkE+DwS0UoEOjmD4gAJH9IAjw0gIKTUG/SBejQC+ZwEPdRWxjgCM0AAdLgMWujKTAwOplEHAywOuyzCDJwRgLRC2PzJEYjEGBDOy3VSihQQogmEUbXOpjxJNQlaQIhd1rzGK9md+VViILjHWJTBETwChIRGdJwCVjwLKgiEZcUAY+gaijWRy4AJE74dZEVCNsWNQxAFGCThGu1CBbGe6WjLLKmSgUxAPkjXz7hGkDCAJMzOTWEAz2CdkyoAOqjkpz/wznhMAHCgCkHoQEosAdd4GWOoG5HkzRas01VQw9KQyH/AHDyGAEjIBBEIAj+uFpnwGtISYew5B2ylDSxtii7YoCzdwNgwI1EVEMLQD+MlwDt9Q9AB2P0kwfFAC6TSD6TcAme8C3S8g+bEAmACQWRcArCNAaB2XJYAJiKeQsKoQjpkAUUAJiGGQQBgDBA8gCGCZgiIAq0kJmRMA4DgQSKGQkigGqdMi620ASRqZhCQAZYcBWmMJoI9g+ZMJqfEQWCCZgCYXSCVIuCpGybYBHSogwlUHi0YhCa4i24Iy3BghDrAl3H6SgZQS2RghTK+Q/nchBkGDWdNjkJMAQdISwG/3Gd2QJdRYlRVUMc4ZIQ2DIRV9Gcg4ZqDOFD/kQtZwErnQIHsyc2ovMlALCQHHEVytmXNNGe8akRvkIuZRIsRhGdyDkRAWg+YhMAzzkRB8AKGMoKXJChrIADGOqhrJAMHIqhoCAQZqChI9qhIjqiOCABHrqhrKAHGQqiKcqhEiABKFqjGbqhOIqhMEqjGNqjI9qjQuqjRPqhLKqjGCoQhmU6oxBBjeAEkSWfCTEI3IlQ/DM61wEklmM39gJjnYaTupV3jZAhGHiVv8lqF5imyhIB10Zm6DRKdtNELbcpBhEHWpN3nbM6ebclDRIBFEY7EYCm2NZpjRCFT/ImZspwGf8CKh55gfKYUIhUKrMnEMaRpkjlITVkEXHgJQGVqYLjp1KTAFtQqoGjBaX6CT4BDKXaqi3wRaX6AXVQqgkAAaWqO5fADaUqDZnmD1fQqqbqD9kQCsRKCqHQAhEwCaZaCoOwBdDhE19wDk2gql9XqkVgDFuQBl1CXMBKrZ+wBeqwbRHgBBgQBEDlc11qPhEQTBcRB6JjOmKqJQwyd8TBAo3wOYpAPP7AAt/yD04yCQMgihBwj2aYBo3wAjODAd4RFwTRJWkQGRsQAXTwMS0wm1eQITRwLQfgHb70Dz9QLI4QkdtAEEICAVjge6TgIhuQAYogCgPwHz4nZOYzORGwIe3/ujqS+qWlIqr+cAH/EFmc8TDD4xNtIBBeUAM1AFB08g86UEr/QAIyQDg+gbCe2AFS0iJthgVIWwM20Ajw9g+9YAqm0KnIIBAOELX/oAURAAiR9QAL4BE01rEfKxC14B3bsAizgLRd9AWM0BfGRh8fURY8sjZANXudg0YY0amYCjRRUwAe0QAxEAM+6wNYMAe+tJf6ejsSwkOXELAS8DE4sBB+9gKK4Ah0wAMN8R9xAXDvJhA05iJ0sBAlUChp6w+fIBB3gLiR8EUCMQ64dGK84A/1YEo9gg7/4Gq0oIsncAKX0IJhACoDEQn08wjJ0whQQJ4H0amg4geckAZ30HMx//CmoloHF8AI7qAJJwBvf4QLHyWEUfADuBYTNrshYeAiVEsHOCAQuhMXChAFUZCy/vAMrtsjE1sG8/APeFC7/EIHKvIPe9GxBaCPQjA5Ixun/qA2/QEG9BMB8kAeNPAl+LWfSKV6ibuvKdAXH8MBsqAN4cAEOesTPOuzw1AAmIBg2EQ88FEQrPcIpBApixAAWpMP7OYRkgAVDCsQmpAh8LYIr/siE0sAXqADIKDAAsHAAkEBjfABxOF7J+YTI7udgvEFXFcCYoME/xAGyDIQz2Y6jzAFBIoQF1ADpdtiigAKr2o6MbwQ53BBYJC/GjCAt2MFqqAK2+YG/foPp4AsVP/rA/cgALqTWAIyyJfhD/DmCE1sARM7BBmwAgGrOwvcBcHlD4AgECmwAwKBYjFQwS4CDBywCDnwiBHrIlEwxbbwfQJxA9NkVNeUEQqAvQQQAi8cwwLxIyAgAxuZuTPhUfnUFw0gEBoQCIwwBlJzv3ADyazrIl/bxP4QuwvRTwo8EwzsEd3Aux7rB6dMYrMVpwhEEBiEAiVQAv4wBmbrcwLhhlsjNhuJErDyB8+6szvhhD77D5fgNKpwzEQ7zKVwCfmjemeARosQBIYMTf5wv0yzsP/gCZfgDKUwOdksNQg1sf+gAOeoO4fzDzzACYvACCPmS4zwA+b8D7XgxcTrEzH/oQMnoBl/QIeLcAXeDA70zAie8Damo481oQin2Ljz2rMCAQCzpQsb+WzxscVO4g+CsQDq5AgkgBocYIH7UMXhc8QN8apfWwKLSgczsQApWLsQQEUD8HFnkAi8uwhze2Kgoso+8Tlz0GU/ewn+kAkXXR9DMDl8tjcL50VedpcmcSxI/Q8N8ggsoAEaAAAVEAs3gA4akAKgcgcnQwyQvTdfANlpAAAnwwKT8Aca4CSckAkaAM9EpAeQ/doD9AmQbQCrUwKQ3QlXANkW6A9fIAVlwAHbsA4+YQMagAYX4AexoAGRUKYCoAFuGAbEYE1dANnudgOawAF4AArfRs8CsQ38/+UhtNM5hIK9GMEo/9CI/szYeodt8bq4UWOMnJMhAJCRpSKmL1w7KIBrMfAruxs1rVN+K5GLPYIdjrAM6206b/oIFBZ0G4ht1Xvg3C0QMwAdXpiTjVBJjZKdFzHHaTfgAkEIMJYhj8BDllhEqxN7a9oj++Ol+oWzT2KmgjipoDepvADfnvqWVeMRJlTN/tAFKGwS1fkPa/YkW1IGZwAJSO4KkHAKSI7kIzACTY7kTB7lVN7kTM4GVQ7lI8DkuQAJSh7lRw4JYe4KXS7mVU7lTD4CZ7DlkHANWE7la47kR+4DUc4GZ/DlUU4Q55IB2x017EoQQ7EROiF36V2eFnEVBv8qESktnVjxxvCJEAtRCY1R2K2lAw6aEevyCn9H5Fx4EEixK50iMyC3EonujBJxl+lCEQ8g1D3CCxGwAWVS6rIoEBu72DpgBJHVEGGwMl0BBEZgBGHwDrIQCwUBBkYgC4Xw6zn0LYsgBkRACTlUJr7uNA1BCI9oEJmwBCvwcQhBAEbwcVs8EGIA7GIgToZufvVxFteOEEDsIcGrNATOGwOKnpsFBndcKgfgCKIgBY+ACSXhB4E3EMPCBw5Q8DhgJDzTAgWfNvTprwDwBgaAAoxHEJWgIQKBxgEfmgCQBtCQACUaHkjRBH6Qz7dhAAngADYAAUGgKYsQDlRA7+c9ETj/cG3/GSuL4C1GqQipwAETB6pTBp5w6Q/BMBAAXxBD0AhADyQm+Q88swh50mILgQAQ0IFNUASdRxQVvyFgAA4YPhCQMAlvK+QgbRAGsAacABpiYsqMfXYGEQ60YBDU5ZwGhVR+cFGxkhBIoQaEXioGgA9VFQGvOROtkPH/MCxJP59M7w8pzaD/kAWkgDsDMPUFkfWGIg1loBNdMQdO+w+WEAGWjp6KsAl+YAbhAAnQKSa5d2LyXBAu//SMWBEFkDXeQUTh0APs6S0lsQmPDKp0sOzl4A+40BVFTxCGry4E0fQSYQe2HyH5t+j/0AGjMANWUATLPhAYUEEekQG+cPX4/+VlWjA3iG8AuacIQUABmgIBL18QcW/q/yAD2wo0oTBS597pGUAGaQKqrz4MZrEI0AAQduAUmAJAwD9H/xQO8VdlwcMZCP8p+mfLH6U5HoQxSkiR0T9wSxQq7ORC4cd/lfzZurRgJCOKCimMGTly0cSRPKr8ExEFpUKKBg4o1PChwr+f/wStWWDNk0RpNaVOhUTK31V/EfxZ8AchSAecU0dWo+SgEa+rWrMmiCb2HxscJEgEMSiV4adOnfoUQjpShr84nz6UShF2kaMElGryOCdVpQEtUcWSEBJWLBBYUv6ZSsBhqoEWnAyEI5FhZCqFh75IMwCBhkLJbvsmRLqii//WRv4aNRrlL9wXGqDA9P1XptmCbomwPsJ6VRsk4jVjxvRzsOaQR0NkI/C36h+Hcx9eKgz5M86amBMrPRrjIw+Sky+18ZCaXmEKAAiMWPEnhjZtAzZAIoVNbLpJoXCoUMgNf1T4JzbZplKDgTyas/CqHMAp4QNBLFArt9ywYsEIoA6MsBVr7PJHu5pM5O5ASyKIqKYveqDRBIUOVKmSf6o44pWpMOBkJBVsYKMmR7KYpAUHHEDBxqSEivCfBEf6QDEIp5QKDBMc2Ooq5i5c7kuu/AGAGVv+MVHLf6pDKKYhGmGxRYVeVEgFf56SiJENSBgJEwA8cKyRW/4BooWdWpz/ozOFDoAlx48cMSCAkUR447MC7JtKEAUVSsClLNmsaRUEdPlAzAvVAkCaAKCzTFQ3r5NzO39uIoCEKxShTSFrUGhAIRouwQTSlGRUaIFLzjiJIgJQGGqRCyJREyhJ/FlmpAeMGSC+f6SMsEosTABAkgdFlS6sjxbxZZdapDl1K60sAOCGSBjoRYfxzB0pB+tGYsiPSQKeJ71FuJsEBX++cHCqHwD45AoAanhVpRkUUYSDEkRQyBGUyACABQOKsAQohXZxxjOFwDAGvp+8dUsRJnKYpJEiVoBNX5umzECBGTYhAAvakkJS3xPUGC8DS9ixxBIV9BwJi6W3gQSl9FDSVaSABZyuKYNtTFNohD80/UeDA7xhAyWUPCF3EZS+cXUkDRR4VaqlLZkBjxx5xDm+hHbV9yax9z5pEe8i9HtasQCv6SPUNtb0QIo4nvJwwH9ac8r0AgIAOw==\"></a></div>\n" +
                "            <div class=\"MainMenu\"><ul class=\"MainMenu\"><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=1\">О компании</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/info.aspx?type=news\">Пресс - центр</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=30\">Информация  для  потребителей</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=34\">Контактная информация</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/feadback.aspx\">Обратная связь</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=99\">Формы и способы оплаты </a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=107\">Закупки и тендеры</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=114\">Абонентская служба</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=124\">Конкурс \"Факел сотрудничества\"</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=125\">Политика обработки персональных данных в ООО \"Газпром межрегионгаз Чебоксары\"</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=1125\">Сведения о задолженности потребителей природного газа (юридических лиц)</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=2125\">Конкурсы и аукционы</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=2134\">Способы передачи показаний приборов учета газа </a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=3134\">Сведения об объектах недвижимого имущества</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=5139\">Политика в области охраны труда и промышленной безопасности</a></li><li class=\"MainMenu_DeActiv\"><a href=\"http://gmch.ru/hierarhy.aspx?id=5145\">Комиссия по корпоративной этике</a></li></ul></div>\n" +
                "        </div>\n" +
                "        <div id=\"ContentBlock\">\n" +
                "            \n" +
                "    <form method=\"post\" action=\"http://gmch.ru/User/\" id=\"Form1\">\n" +
                "\t<div class=\"aspNetHidden\">\n" +
                "\t<input type=\"hidden\" name=\"__EVENTTARGET\" id=\"__EVENTTARGET\" value=\"\">\n" +
                "\t<input type=\"hidden\" name=\"__EVENTARGUMENT\" id=\"__EVENTARGUMENT\" value=\"\">\n" +
                "\t<input type=\"hidden\" name=\"__VIEWSTATE\" id=\"__VIEWSTATE\" value=\"/wEPDwUJNTc2MTIxMzY5DxYCHhNWYWxpZGF0ZVJlcXVlc3RNb2RlAgEWAmYPZBYCAgMPZBYEAgEPZBYCZg8WAh4EVGV4dAXFEDx1bCBjbGFzcz1NYWluTWVudT48bGkgY2xhc3M9TWFpbk1lbnVfRGVBY3Rpdj48YSBocmVmPSIuLi9oaWVyYXJoeS5hc3B4P2lkPTEiPtCeINC60L7QvNC/0LDQvdC40Lg8L2E+PC9saT48bGkgY2xhc3M9TWFpbk1lbnVfRGVBY3Rpdj48YSBocmVmPSIuLi9pbmZvLmFzcHg/dHlwZT1uZXdzIj7Qn9GA0LXRgdGBIC0g0YbQtdC90YLRgDwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg/aWQ9MzAiPtCY0L3RhNC+0YDQvNCw0YbQuNGPICDQtNC70Y8gINC/0L7RgtGA0LXQsdC40YLQtdC70LXQuTwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg/aWQ9MzQiPtCa0L7QvdGC0LDQutGC0L3QsNGPINC40L3RhNC+0YDQvNCw0YbQuNGPPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vZmVhZGJhY2suYXNweCI+0J7QsdGA0LDRgtC90LDRjyDRgdCy0Y/Qt9GMPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD05OSI+0KTQvtGA0LzRiyDQuCDRgdC/0L7RgdC+0LHRiyDQvtC/0LvQsNGC0YsgPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMDciPtCX0LDQutGD0L/QutC4INC4INGC0LXQvdC00LXRgNGLPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMTQiPtCQ0LHQvtC90LXQvdGC0YHQutCw0Y8g0YHQu9GD0LbQsdCwPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMjQiPtCa0L7QvdC60YPRgNGBICLQpNCw0LrQtdC7INGB0L7RgtGA0YPQtNC90LjRh9C10YHRgtCy0LAiPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0xMjUiPtCf0L7Qu9C40YLQuNC60LAg0L7QsdGA0LDQsdC+0YLQutC4INC/0LXRgNGB0L7QvdCw0LvRjNC90YvRhSDQtNCw0L3QvdGL0YUg0LIg0J7QntCeICLQk9Cw0LfQv9GA0L7QvCDQvNC10LbRgNC10LPQuNC+0L3Qs9Cw0Lcg0KfQtdCx0L7QutGB0LDRgNGLIjwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg/aWQ9MTEyNSI+0KHQstC10LTQtdC90LjRjyDQviDQt9Cw0LTQvtC70LbQtdC90L3QvtGB0YLQuCDQv9C+0YLRgNC10LHQuNGC0LXQu9C10Lkg0L/RgNC40YDQvtC00L3QvtCz0L4g0LPQsNC30LAgKNGO0YDQuNC00LjRh9C10YHQutC40YUg0LvQuNGGKTwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg/aWQ9MjEyNSI+0JrQvtC90LrRg9GA0YHRiyDQuCDQsNGD0LrRhtC40L7QvdGLPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0yMTM0Ij7QodC/0L7RgdC+0LHRiyDQv9C10YDQtdC00LDRh9C4INC/0L7QutCw0LfQsNC90LjQuSDQv9GA0LjQsdC+0YDQvtCyINGD0YfQtdGC0LAg0LPQsNC30LAgPC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD0zMTM0Ij7QodCy0LXQtNC10L3QuNGPINC+0LEg0L7QsdGK0LXQutGC0LDRhSDQvdC10LTQstC40LbQuNC80L7Qs9C+INC40LzRg9GJ0LXRgdGC0LLQsDwvYT48L2xpPjxsaSBjbGFzcz1NYWluTWVudV9EZUFjdGl2PjxhIGhyZWY9Ii4uL2hpZXJhcmh5LmFzcHg/aWQ9NTEzOSI+0J/QvtC70LjRgtC40LrQsCDQsiDQvtCx0LvQsNGB0YLQuCDQvtGF0YDQsNC90Ysg0YLRgNGD0LTQsCDQuCDQv9GA0L7QvNGL0YjQu9C10L3QvdC+0Lkg0LHQtdC30L7Qv9Cw0YHQvdC+0YHRgtC4PC9hPjwvbGk+PGxpIGNsYXNzPU1haW5NZW51X0RlQWN0aXY+PGEgaHJlZj0iLi4vaGllcmFyaHkuYXNweD9pZD01MTQ1Ij7QmtC+0LzQuNGB0YHQuNGPINC/0L4g0LrQvtGA0L/QvtGA0LDRgtC40LLQvdC+0Lkg0Y3RgtC40LrQtTwvYT48L2xpPjwvdWw+ZAIDD2QWAgIBD2QWDAIBDw8WAh4HVmlzaWJsZWhkZAIDD2QWAgIBDxYCHwEFCDYxMjc3NjAxZAIFDxYCHwEFQtCf0YDQuNC10Lwg0L/QvtC60LDQt9Cw0L3QuNC5INC/0YDQuNCx0L7RgNC+0LIg0YPRh9C10YLQsCDQs9Cw0LfQsGQCBw8PFgIfAmhkZAIJD2QWCgIBDxYCHwEFwgQ8cD7QlNC70Y8g0L7QsdC10YHQv9C10YfQtdC90LjRjyDQutC+0L3RhNC40LTQtdC90YbQuNCw0LvRjNC90L7RgdGC0Lgg0LTQsNC90L3Ri9GFLCDQtNC+0L/QvtC70L3QuNGC0LXQu9GM0L3QviDQvdGD0LbQvdC+INCy0YvQsdGA0LDRgtGMINC00L7QvCwg0LrQvtGA0L/Rg9GBICjQv9GA0Lgg0L3QsNC70LjRh9C40LgpLCDQvdC+0LzQtdGAINC60LLQsNGA0YLQuNGA0YsgKNC/0YDQuCDQvdCw0LvQuNGH0LjQuCksICDQv9C+0YHQu9C1INGH0LXQs9C+INC/0L7QtNGC0LLQtdGA0LTQuNGC0Ywg0LDQtNGA0LXRgS48L3A+PGRpdiBjbGFzcz0iSW5wdXRMaW5lIj48ZGl2IGNsYXNzPSJGaWxkQ2FwdGlvbkxlZnQiPtCd0LDRgdC10LvQtdC90L3Ri9C5INC/0YPQvdC60YI6PC9kaXY+PGRpdiBjbGFzcz0iRmlsZElucHV0Ij4g0LMg0KfQtdCx0L7QutGB0LDRgNGLPC9kaXY+PC9kaXY+PGRpdiBjbGFzcz0iSW5wdXRMaW5lIj48ZGl2IGNsYXNzPSJGaWxkQ2FwdGlvbkxlZnQiPtCj0LvQuNGG0LA6PC9kaXY+PGRpdiBjbGFzcz0iRmlsZElucHV0Ij4g0YPQuyDQny7Qki7QlNC10LzQtdC90YLRjNC10LLQsDwvZGl2PjwvZGl2PmQCAw9kFgJmDw8WBB4IQ3NzQ2xhc3MFCUlucHV0TGluZR4EXyFTQgICZBYEAgEPDxYEHwMFD0ZpbGRDYXB0aW9uTGVmdB8EAgJkFgJmDxYCHwEFFtCS0LLQtdC00LjRgtC1INC00L7QvDpkAgMPZBYCZg8QZBAVEBXQstGL0LHRgNCw0YLRjCDQtNC+0LwBMAExATIBMwE0ATYBNwE5AjE0AjE1AjE4AjE5AjIwAjIyAjI0FRAAATABMQEyATMBNAE2ATcBOQIxNAIxNQIxOAIxOQIyMAIyMgIyNBQrAxBnZ2dnZ2dnZ2dnZ2dnZ2dnZGQCBQ9kFgJmDw8WBB8DBQlJbnB1dExpbmUfBAICZBYEAgEPDxYEHwMFD0ZpbGRDYXB0aW9uTGVmdB8EAgJkFgJmDxYCHwEFHNCj0LrQsNC20LjRgtC1INC60L7RgNC/0YPRgTpkAgMPZBYCZg8QZA8WAWYWARAFF9Cd0LXRgiDQt9C90LDRh9C10L3QuNC5BQEwZ2RkAgcPZBYCZg8PFgQfAwUJSW5wdXRMaW5lHwQCAmQWBAIBDw8WBB8DBQ9GaWxkQ2FwdGlvbkxlZnQfBAICZBYCZg8WAh8BBSDQo9C60LDQttC40YLQtSDQutCy0LDRgNGC0LjRgNGDOmQCAw9kFgJmDxBkDxYBZhYBEAUX0J3QtdGCINC30L3QsNGH0LXQvdC40LkFATBnZGQCCQ9kFgJmDw8WBB8DBQlJbnB1dExpbmUfBAICZBYEAgEPDxYEHwMFD0ZpbGRDYXB0aW9uTGVmdB8EAgJkFgJmDxYCHwEFHtCj0LrQsNC20LjRgtC1INC60L7QvNC90LDRgtGDOmQCAw9kFgJmDxBkDxYBZhYBEAUX0J3QtdGCINC30L3QsNGH0LXQvdC40LkFATBnZGQCDQ8PFgIfAmhkFgICBQ8WAh8BBfMFPHAgY2xhc3M9Ik1zb05vcm1hbCIgc3R5bGU9Im1hcmdpbjogNC44NXB0IDBjbTsgdGV4dC1hbGlnbjoganVzdGlmeTsiPjxzdHJvbmc+PHNwYW4gc3R5bGU9ImZvbnQtc2l6ZTogMTQuMHB0OyBmb250LWZhbWlseTogJnF1b3Q7QXJpYWwmcXVvdDssJnF1b3Q7c2Fucy1zZXJpZiZxdW90OzsgbXNvLWZhcmVhc3QtZm9udC1mYW1pbHk6ICZxdW90O1RpbWVzIE5ldyBSb21hbiZxdW90OzsgY29sb3I6ICM0ODUwNTY7IG1zby1mYXJlYXN0LWxhbmd1YWdlOiBSVTsiPtCSINC00LDQvdC90L7QvCDRgNCw0LfQtNC10LvQtSDQsNCx0L7QvdC10L3RgtGLIC0g0L/QvtGB0LXRgtC40YLQtdC70Lgg0YHQsNC50YLQsCDQuNC80LXRjtGCINCy0L7Qt9C80L7QttC90L7RgdGC0Ywg0YPQt9C90LDRgtGMINC30LDQtNC+0LvQttC10L3QvdC+0YHRgtGMINC30LAg0LPQsNC30L7RgdC90LDQsdC20LXQvdC40LUg0L3QsCDQvdCw0YfQsNC70L4g0YLQtdC60YPRidC10LPQviDRgNCw0YHRh9C10YLQvdC+0LPQviDQvNC10YHRj9GG0LAsINC/0YDQvtGB0LzQvtGC0YDQtdGC0YwgJm5ic3A70L/QvtC60LDQt9Cw0L3QuNGPINC/0YDQuNCx0L7RgNC+0LIg0YPRh9C10YLQsCDQs9Cw0LfQsCZuYnNwOyjQutGD0LEu0LwuKSZuYnNwO9C30LAg0L/RgNC10LTRi9C00YPRidC40Lkg0LzQtdGB0Y/RhiDQuCDQstCy0LXRgdGC0Lgg0L/QvtC60LDQt9Cw0L3QuNGPINC90LAg0YLQtdC60YPRidGD0Y4g0LTQsNGC0YMuJm5ic3A7PC9zcGFuPjwvc3Ryb25nPjwvcD5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBRljdGwwMCRCb2R5Q29udGVuZXIkU2F2ZU1lSDpN6gNpYugrpZrOEm8H5qiXtgAuP3LNtxZMlkdNuAQ=\">\n" +
                "\t</div>\n" +
                "\t\n" +
                "\t<div class=\"aspNetHidden\">\n" +
                "\n" +
                "\t\t<input type=\"hidden\" name=\"__VIEWSTATEGENERATOR\" id=\"__VIEWSTATEGENERATOR\" value=\"70142F31\">\n" +
                "\t</div>\t\n" +
                "        <div class=\"UserContent\">\n" +
                "            <div class=\"LeftBlock\">\n" +
                "                \n" +
                "                <div id=\"BodyContener_UserGuidInfo\" class=\"UserGuidInfo\">\n" +
                "\t\t\n" +
                "                    <div>Вы зашли<br>под номером</div>\n" +
                "                    <div class=\"UserGuidNum\">61277601</div>\n" +
                "                    <div><img alt=\"\" title=\"\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAABDCAYAAACWXosQAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABrxJREFUeNrsXO9x6jgQFx6+nzuIqSBKBThzn2/CqwDSwD2ogJcKCNcAUEHM3Oc38CoIqQBfBefr4KSb9TvFsS3JWq1tyM54SIJja/Xb/1ppwIjp19//DMXHRlyTwldHce3Ftf3+x28Z6zAJHrj4iMUVKn9OJQ9i7Knv9w+ImIzERwS/rsTFa26XgK3F9dw18AQfM/GxVHgpIyl8C/hZgppiAznwrFFzcU01TFaRZPRRMHzsiGZtNMKm42UteHnuLGiCyQkwGSI8TgK3bRmwAxIvUgC/uFqQwJMJeUFiUtIGntl3wBj4wZXrQwJkJiOMQZXQCp5NTRtEwHKagSXqjKYtPTCZO/QNsZZ9c/BhWiHshE+DwONvz3N5TxGYAC9nTwKYk/RtSduaFhMowJRI0eaeAXPiBRM0TjCZEyLQpl3mJWD9ohAiOqpCgG8zHF8DaIzAbFFGqVcDWicnsiHdfoJ2JVYDEzSq4m527VqNAppwqHPCcDy6dvUMEADjTL/cgkkvniPI9OJBI8ydqCLIqwDtk3oI2vHC5uRE+K6sLdDSS5pY4haHUyugQf8DpbYlXW/86YtPeyIc8/qC5p+3BhqscW0JmHz2vZ5GvELebkVETOaj+Ljz5OOkObwT71h0VfodhGTSGmgA3MmTqUzg2RT0ldhEPnQhT0sYfn1wRyT1M0Zb5Zc0s11XQwcNIjtMbTsRNqyuGD3J+Yra1jQGnbRY5owyMr1ntKsIR/DV29ZBA3rEYKppx5KDTx4R5J3yPbKz7L5Jn7/XDRiwZLNyMBt3FLtQavybbrNFE81auwrigIB52WQ6a6Kpbfbww9hlHjVxBC+DHHaHFQFTbXWS2jbvE2AlPHAA8MEyl3sSvHzDHAvJ0gwkxfcGibeUyi9dAyz3dzD5i7bHMiRkWtrzEfiKBzA3HICUZmPPPovB3QJNAW/LaOqUXalgyPv7Zx4vhcCvzS3/jUMUjUaDTyisAHPZYIgWXPnYCRp5mrSoZdxcd4Rumvbue/NpMKkv7P3ufkySrXMyYFm0FKwcmVvnWYZVQx0igJWfYvAVJDGruTc2YKwqAeVwxeI5Tz7TAuApKoxl5whaUiLkWRMBHDpq1kwBK6dYE0nVOeUnVlJoLoAdgalZwv3OaQLwIgEZg2BEYC1OStSbiPsyBxNZXF6S7zjAM/OU52hSNRlYMpefVDPVVAVGZTVDkOBXVl4SkvfflQEAud1GI8U506kFP7nQ8ZIkf1Qci0NJTh4AMyp5/2vJu+X4ZR9M5clFQ0OQxuzjsUK1YW5Z9UMOQjxzAb6vSHW+SrclaJKbLvH8FPzPjyoQIQSv29S/rhjLviFoVQXidYkwSoGWZb+lGGfpyUVBGVBSosR1Bq1YwYTYmAVek1wn7OPSR6KpfNvU+nKzLSfjLKVZnlRQiD5DDT/bmrGnCKZRLTRUPS8EwXotxgKBarrEdQCgZsxtSWKsy1mKWqa53yVU5sD8GQrXDGqIVet9W42JtV1WOWn81NpACA/qATiB4mvODK8/wrajKdOYaCyag2DmUn5sMIk7DC0raLVJIPXz5KJc01YMdydKCIJQJz2mIGMn1bEitUWAtNEbfG+zLpZonpcx81qsBI7noM0YPnGL7yJErTWhqZIwF1MODO1RfbWJD7Tpml4FHjfo1Zna0AK0sS+BKkRlqUXFwtSv7U1uAmBNtS0eSnUXwPkA7dbiuxtiTTspSbXqPw+a//vv2D85yeLeRFMhyVRwIceLLATZXxmroS8y0jTwiyHRGCMD/7lUIt29BrSflRrIC9FckM/1NG5hOjmhlrlGn7FiIjOdaQRtXmIOwusiqMVSROghP2uUYhhGcCFoUVJTtsq/Qz8zMqiIorxpWxWQFQHRradxvTlostScuSbQSICniQ/B891ucGOhVSFBjtbY+Rd9mxSymrLWTjnKnvUNNG7hp+Ie+LR3ZrIi/M/LVtgFiw+g+dr7VQbEL8j+sHHIr0k1tAIJUeG6RMtiTwWLdyH/Px6DEV4oDXHDJDq/T9uhC5N0sBhWhmR+l6BpJ2W8Sc1YFgYKMtUBTtH3GBUGGhn6lxvWfcr91g5AS1j9Col2r52JhRl6No9MYUYHGq/4fSoYGRtMnjEpE4cR6MTs/1rkmyYnmxqAMjYFzWd301iRokgjZaFSD4wtKhU+qjY2tALB1HUfo/g5ig5jbjFJvCZn67qZ5FTRblAwF14YUjSMG0q+T+aPJlrfotZ2QtNUhkLD+yLCMfWNTipoqccX5f5JV5a6MXXGDpSyflNKBdqtpab5NI9/9aDaUkdv/wowABwsfnqtRlOTAAAAAElFTkSuQmCC\"></div>\n" +
                "                    <div class=\"LogOutBlock\"><a id=\"BodyContener_LogOut\" href=\"javascript:__doPostBack(&#39;ctl00$BodyContener$LogOut&#39;,&#39;&#39;)\">Выход</a></div>\n" +
                "                \n" +
                "\t</div>\n" +
                "            </div>\n" +
                "            <div class=\"UserContent\">\n" +
                "                <div class=\"RazdelCaption\">Прием показаний приборов учета газа</div>\n" +
                "                \n" +
                "                <div id=\"BodyContener_Messege\">\n" +
                "\t\t\n" +
                "                    <p>Для обеспечения конфиденциальности данных, дополнительно нужно выбрать дом, корпус (при наличии), номер квартиры (при наличии),  после чего подтвердить адрес.</p><div class=\"InputLine\"><div class=\"FildCaptionLeft\">Населенный пункт:</div><div class=\"FildInput\"> г Чебоксары</div></div><div class=\"InputLine\"><div class=\"FildCaptionLeft\">Улица:</div><div class=\"FildInput\"> ул П.В.Дементьева</div></div>\n" +
                "                    <div id=\"HouseFild\" class=\"InputLine\">\n" +
                "\t\t\t\n" +
                "    <div id=\"BodyContener_HouseFild_CaptionBlock\" class=\"FildCaptionLeft\">\n" +
                "\t\t\t\tВведите дом:\n" +
                "\t\t\t</div>\n" +
                "    <div id=\"BodyContener_HouseFild_TextFildBlock\" class=\"FildInput\">\n" +
                "\t\t\t\t<select name=\"ctl00$BodyContener$HouseFild$SelectFild\" id=\"BodyContener_HouseFild_SelectFild\">\n" +
                "\t\t\t\t\t<option value=\"\">выбрать дом</option>\n" +
                "\t\t\t\t\t<option value=\"0\">0</option>\n" +
                "\t\t\t\t\t<option value=\"1\">1</option>\n" +
                "\t\t\t\t\t<option value=\"2\">2</option>\n" +
                "\t\t\t\t\t<option value=\"3\">3</option>\n" +
                "\t\t\t\t\t<option value=\"4\">4</option>\n" +
                "\t\t\t\t\t<option value=\"6\">6</option>\n" +
                "\t\t\t\t\t<option value=\"7\">7</option>\n" +
                "\t\t\t\t\t<option value=\"9\">9</option>\n" +
                "\t\t\t\t\t<option value=\"14\">14</option>\n" +
                "\t\t\t\t\t<option value=\"15\">15</option>\n" +
                "\t\t\t\t\t<option value=\"18\">18</option>\n" +
                "\t\t\t\t\t<option value=\"19\">19</option>\n" +
                "\t\t\t\t\t<option value=\"20\">20</option>\n" +
                "\t\t\t\t\t<option value=\"22\">22</option>\n" +
                "\t\t\t\t\t<option value=\"24\">24</option>\n" +
                "\n" +
                "\t\t\t\t</select>\n" +
                "\t\t\t</div>\n" +
                "\n" +
                "\t\t</div>\n" +
                "                    <div id=\"HouseCaseFild\" class=\"InputLine\" style=\"display: block;\">\n" +
                "\t\t\t\n" +
                "    <div id=\"BodyContener_HouseCaseFild_CaptionBlock\" class=\"FildCaptionLeft\">\n" +
                "\t\t\t\tУкажите корпус:\n" +
                "\t\t\t</div>\n" +
                "    <div id=\"BodyContener_HouseCaseFild_TextFildBlock\" class=\"FildInput\">\n" +
                "\t\t\t\t<select name=\"ctl00$BodyContener$HouseCaseFild$SelectFild\" id=\"BodyContener_HouseCaseFild_SelectFild\" style=\"display: block;\"><option value=\"\">Выбрать</option><option value=\"\">Отсутствует</option><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option></select>\n" +
                "\t\t\t</div>\n" +
                "\n" +
                "\t\t</div>\n" +
                "                    <div id=\"ApartmentFild\" class=\"InputLine\" style=\"display: block;\">\n" +
                "\t\t\t\n" +
                "    <div id=\"BodyContener_ApartmentFild_CaptionBlock\" class=\"FildCaptionLeft\">\n" +
                "\t\t\t\tУкажите квартиру:\n" +
                "\t\t\t</div>\n" +
                "    <div id=\"BodyContener_ApartmentFild_TextFildBlock\" class=\"FildInput\">\n" +
                "\t\t\t\t<select name=\"ctl00$BodyContener$ApartmentFild$SelectFild\" id=\"BodyContener_ApartmentFild_SelectFild\" style=\"display: block;\"><option value=\"\">Выбрать</option><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option><option value=\"13\">13</option><option value=\"14\">14</option><option value=\"15\">15</option><option value=\"16\">16</option><option value=\"17\">17</option><option value=\"18\">18</option><option value=\"19\">19</option><option value=\"20\">20</option><option value=\"21\">21</option><option value=\"22\">22</option><option value=\"23\">23</option><option value=\"24\">24</option><option value=\"25\">25</option><option value=\"26\">26</option><option value=\"27\">27</option><option value=\"28\">28</option><option value=\"29\">29</option><option value=\"30\">30</option><option value=\"31\">31</option><option value=\"32\">32</option><option value=\"33\">33</option><option value=\"34\">34</option><option value=\"35\">35</option><option value=\"36\">36</option><option value=\"37\">37</option><option value=\"38\">38</option><option value=\"39\">39</option><option value=\"40\">40</option><option value=\"41\">41</option><option value=\"42\">42</option><option value=\"43\">43</option><option value=\"44\">44</option><option value=\"45\">45</option><option value=\"46\">46</option><option value=\"47\">47</option><option value=\"48\">48</option><option value=\"49\">49</option><option value=\"50\">50</option><option value=\"51\">51</option><option value=\"52\">52</option><option value=\"53\">53</option><option value=\"54\">54</option><option value=\"55\">55</option><option value=\"56\">56</option><option value=\"57\">57</option><option value=\"58\">58</option><option value=\"59\">59</option><option value=\"60\">60</option><option value=\"61\">61</option><option value=\"62\">62</option><option value=\"63\">63</option><option value=\"64\">64</option><option value=\"65\">65</option><option value=\"66\">66</option><option value=\"67\">67</option><option value=\"68\">68</option><option value=\"69\">69</option><option value=\"70\">70</option><option value=\"71\">71</option><option value=\"72\">72</option><option value=\"73\">73</option><option value=\"74\">74</option><option value=\"75\">75</option><option value=\"76\">76</option><option value=\"77\">77</option><option value=\"78\">78</option><option value=\"79\">79</option><option value=\"80\">80</option><option value=\"81\">81</option><option value=\"82\">82</option><option value=\"83\">83</option><option value=\"84\">84</option><option value=\"85\">85</option><option value=\"86\">86</option><option value=\"87\">87</option><option value=\"88\">88</option><option value=\"89\">89</option><option value=\"90\">90</option><option value=\"91\">91</option><option value=\"92\">92</option><option value=\"93\">93</option><option value=\"94\">94</option><option value=\"95\">95</option><option value=\"96\">96</option><option value=\"97\">97</option><option value=\"98\">98</option><option value=\"99\">99</option><option value=\"100\">100</option><option value=\"101\">101</option><option value=\"102\">102</option><option value=\"103\">103</option><option value=\"104\">104</option><option value=\"105\">105</option><option value=\"106\">106</option><option value=\"107\">107</option><option value=\"108\">108</option><option value=\"109\">109</option><option value=\"110\">110</option><option value=\"111\">111</option><option value=\"112\">112</option><option value=\"113\">113</option><option value=\"114\">114</option><option value=\"115\">115</option><option value=\"116\">116</option><option value=\"117\">117</option><option value=\"118\">118</option><option value=\"119\">119</option><option value=\"120\">120</option><option value=\"121\">121</option><option value=\"122\">122</option><option value=\"123\">123</option><option value=\"124\">124</option><option value=\"125\">125</option><option value=\"126\">126</option><option value=\"127\">127</option><option value=\"128\">128</option><option value=\"129\">129</option><option value=\"130\">130</option><option value=\"131\">131</option><option value=\"132\">132</option><option value=\"133\">133</option><option value=\"134\">134</option><option value=\"135\">135</option><option value=\"136\">136</option><option value=\"137\">137</option><option value=\"138\">138</option><option value=\"139\">139</option><option value=\"140\">140</option><option value=\"141\">141</option><option value=\"142\">142</option><option value=\"143\">143</option><option value=\"144\">144</option><option value=\"145\">145</option><option value=\"146\">146</option><option value=\"147\">147</option><option value=\"148\">148</option><option value=\"149\">149</option><option value=\"150\">150</option><option value=\"151\">151</option><option value=\"152\">152</option><option value=\"153\">153</option><option value=\"154\">154</option><option value=\"155\">155</option><option value=\"156\">156</option><option value=\"157\">157</option><option value=\"158\">158</option><option value=\"159\">159</option><option value=\"160\">160</option><option value=\"161\">161</option><option value=\"162\">162</option><option value=\"163\">163</option><option value=\"164\">164</option><option value=\"165\">165</option><option value=\"166\">166</option><option value=\"167\">167</option><option value=\"168\">168</option><option value=\"169\">169</option><option value=\"170\">170</option><option value=\"171\">171</option><option value=\"172\">172</option><option value=\"173\">173</option><option value=\"174\">174</option><option value=\"175\">175</option><option value=\"176\">176</option><option value=\"177\">177</option><option value=\"178\">178</option><option value=\"179\">179</option><option value=\"180\">180</option><option value=\"181\">181</option><option value=\"182\">182</option><option value=\"183\">183</option><option value=\"184\">184</option><option value=\"185\">185</option><option value=\"186\">186</option><option value=\"187\">187</option><option value=\"188\">188</option><option value=\"189\">189</option><option value=\"190\">190</option><option value=\"191\">191</option><option value=\"192\">192</option><option value=\"193\">193</option><option value=\"194\">194</option><option value=\"195\">195</option><option value=\"196\">196</option><option value=\"197\">197</option><option value=\"198\">198</option><option value=\"199\">199</option><option value=\"200\">200</option><option value=\"201\">201</option><option value=\"202\">202</option><option value=\"203\">203</option><option value=\"204\">204</option><option value=\"205\">205</option><option value=\"206\">206</option><option value=\"207\">207</option><option value=\"208\">208</option><option value=\"209\">209</option><option value=\"210\">210</option><option value=\"211\">211</option><option value=\"212\">212</option><option value=\"213\">213</option><option value=\"214\">214</option><option value=\"215\">215</option><option value=\"216\">216</option><option value=\"217\">217</option><option value=\"218\">218</option><option value=\"219\">219</option><option value=\"220\">220</option><option value=\"221\">221</option><option value=\"222\">222</option><option value=\"223\">223</option><option value=\"224\">224</option><option value=\"225\">225</option><option value=\"226\">226</option><option value=\"227\">227</option><option value=\"228\">228</option><option value=\"229\">229</option><option value=\"230\">230</option><option value=\"231\">231</option><option value=\"232\">232</option><option value=\"233\">233</option><option value=\"234\">234</option><option value=\"235\">235</option><option value=\"236\">236</option><option value=\"237\">237</option><option value=\"238\">238</option><option value=\"239\">239</option><option value=\"240\">240</option><option value=\"241\">241</option><option value=\"242\">242</option><option value=\"243\">243</option><option value=\"244\">244</option><option value=\"245\">245</option><option value=\"246\">246</option><option value=\"247\">247</option><option value=\"248\">248</option><option value=\"249\">249</option><option value=\"250\">250</option><option value=\"251\">251</option><option value=\"252\">252</option><option value=\"253\">253</option><option value=\"254\">254</option><option value=\"255\">255</option><option value=\"256\">256</option><option value=\"257\">257</option><option value=\"258\">258</option><option value=\"259\">259</option><option value=\"260\">260</option><option value=\"261\">261</option><option value=\"262\">262</option><option value=\"263\">263</option><option value=\"264\">264</option><option value=\"265\">265</option><option value=\"266\">266</option><option value=\"267\">267</option><option value=\"268\">268</option><option value=\"269\">269</option><option value=\"270\">270</option><option value=\"271\">271</option><option value=\"272\">272</option><option value=\"273\">273</option><option value=\"274\">274</option><option value=\"275\">275</option><option value=\"276\">276</option><option value=\"277\">277</option><option value=\"278\">278</option><option value=\"279\">279</option><option value=\"280\">280</option><option value=\"281\">281</option><option value=\"282\">282</option><option value=\"283\">283</option><option value=\"284\">284</option><option value=\"285\">285</option><option value=\"286\">286</option><option value=\"287\">287</option><option value=\"288\">288</option><option value=\"289\">289</option><option value=\"290\">290</option><option value=\"291\">291</option><option value=\"292\">292</option><option value=\"293\">293</option><option value=\"294\">294</option><option value=\"295\">295</option><option value=\"296\">296</option><option value=\"297\">297</option><option value=\"298\">298</option><option value=\"299\">299</option><option value=\"300\">300</option></select>\n" +
                "\t\t\t</div>\n" +
                "\n" +
                "\t\t</div>\n" +
                "                    <div id=\"RoomFild\" class=\"InputLine\" style=\"display: none;\">\n" +
                "\t\t\t\n" +
                "    <div id=\"BodyContener_RoomFild_CaptionBlock\" class=\"FildCaptionLeft\">\n" +
                "\t\t\t\tУкажите комнату:\n" +
                "\t\t\t</div>\n" +
                "    <div id=\"BodyContener_RoomFild_TextFildBlock\" class=\"FildInput\">\n" +
                "\t\t\t\t<select name=\"ctl00$BodyContener$RoomFild$SelectFild\" id=\"BodyContener_RoomFild_SelectFild\"></select>\n" +
                "\t\t\t</div>\n" +
                "\n" +
                "\t\t</div>\n" +
                "                    <input type=\"hidden\" name=\"ctl00$BodyContener$ResultHouse\" id=\"ResultHouse\" value=\"18\">\n" +
                "                    <input type=\"hidden\" name=\"ctl00$BodyContener$ResultHouseCase\" id=\"ResultHouseCase\" value=\"3\">\n" +
                "                    <input type=\"hidden\" name=\"ctl00$BodyContener$ResultApartment\" id=\"ResultApartment\" value=\"\">\n" +
                "                    <input type=\"hidden\" name=\"ctl00$BodyContener$ResultRoom\" id=\"ResultRoom\" value=\"\">\n" +
                "                    <div class=\"SaveMeInput\"><input id=\"BodyContener_SaveMe\" type=\"checkbox\" name=\"ctl00$BodyContener$SaveMe\"><label for=\"BodyContener_SaveMe\">Запомнить номер счета и заходить в раздел «Прием показаний приборов учета газа» без его ввода.</label></div>\n" +
                "                    <div class=\"BtnBlock\">\n" +
                "                        <a id=\"BodyContener_BtnLogIn\" href=\"javascript:__doPostBack(&#39;ctl00$BodyContener$BtnLogIn&#39;,&#39;&#39;)\">Подтвердить <br> адрес</a>\n" +
                "                        <a id=\"BodyContener_BtnLogOut\" href=\"javascript:__doPostBack(&#39;ctl00$BodyContener$BtnLogOut&#39;,&#39;&#39;)\">Вернуться к вводу <br> лицевого счета</a>\n" +
                "                    </div>\n" +
                "                \n" +
                "\t</div>\n" +
                "                \n" +
                "                \n" +
                "            </div>\n" +
                "        </div>\n" +
                "        \n" +
                "        \n" +
                "        \n" +
                "\n" +
                "    </form>\n" +
                "\n" +
                "        </div>\n" +
                "        <div id=\"BottomBlock\">\n" +
                "            <div id=\"Copiright\">©  2011, <b>ООО «Газпром межрегионгаз Чебоксары»</b><br>\n" +
                "                428031, Чувашская Республика, г. Чебоксары, ул. 324-й Стрелковой дивизии, 28 А<br>\n" +
                "                Электронная почта: <a href=\"mailto:info@gmch.ru\" title=\"\">info@gmch.ru</a></div>\n" +
                "            <div id=\"Razrabotka\"></div>\n" +
                "        </div>\n" +
                "    \n" +
                "</div>\n" +
                "\n" +
                "    <!-- Yandex.Metrika counter --> <script type=\"text/javascript\"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter34905725 = new Ya.Metrika({ id:34905725, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName(\"script\")[0], s = d.createElement(\"script\"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = \"text/javascript\"; s.async = true; s.src = \"https://mc.yandex.ru/metrika/watch.js\"; if (w.opera == \"[object Opera]\") { d.addEventListener(\"DOMContentLoaded\", f, false); } else { f(); } })(document, window, \"yandex_metrika_callbacks\"); </script> <noscript>&lt;div&gt;&lt;img src=\"https://mc.yandex.ru/watch/34905725\" style=\"position:absolute; left:-9999px;\" alt=\"\" /&gt;&lt;/div&gt;</noscript> <!-- /Yandex.Metrika counter -->\n" +
                "\n" +
                "\n" +
                "</body></html>";
        Document doc = Jsoup.parse(testPage);
        Elements e2 = doc.select("#BodyContener_Messege > .InputLine > .FildInput");
        AddressData addressData = new AddressData();
        addressData.reset();
        if(e2.size()>=6) {
            addressData.locality = e2.get(0).text(); //населенный пункт
            addressData.street = e2.get(1).text(); //улица
            Element houses = e2.get(2).children().first();
            if (houses != null) {
                Elements childs = houses.children();
                for (int i = 0; i < childs.size(); i++) {
                    if (Element.class.isInstance(childs.get(i))) {
                        Element el = childs.get(i);
                        if (el.attr("value") != "")
                            addressData.houseList.add(el.attr("value"));
                    }
                }
            }
            Element corps = e2.get(3).children().first();
            if (corps != null) {
                Elements childs = corps.children();
                for (int i = 0; i < childs.size(); i++) {
                    if (Element.class.isInstance(childs.get(i))) {
                        Element el = childs.get(i);
                        if (el.attr("value") != "")
                            addressData.corpusList.add(el.attr("value"));
                    }
                }
            }
            System.out.print("end\n");
        }
    }
}
