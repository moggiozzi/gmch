package com.example.user.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class AddressActivity extends AppCompatActivity {
    private MyApplication myApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        myApp = (MyApplication)getApplication();

        AddressData addressData = myApp.userData.addressData;
        TextView textView = (TextView) findViewById(R.id.locality);
        textView.setText(addressData.locality);
        textView = (TextView) findViewById(R.id.street);
        textView.setText(addressData.street);
        Spinner houseSpinner = (Spinner)findViewById(R.id.house_spinner);
        ArrayAdapter<String> adp= new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,addressData.houseList);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        houseSpinner.setAdapter(adp);
        houseSpinner.setPrompt("Дом");
        for(int i=0;i<addressData.houseList.size();i++) {
            if(addressData.houseList.get(i).equals(addressData.house)) {
                houseSpinner.setSelection(i);
                break;
            }
        }
        EditText editText = (EditText)findViewById(R.id.corpus_value);
        editText.setText(addressData.corpus);

        editText = (EditText)findViewById(R.id.apartment_value);
        editText.setText(addressData.appartment);
/**
        Spinner corpusSpinner = (Spinner)findViewById(R.id.corpus_spinner);
        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,addressData.corpusList);
        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        corpusSpinner.setAdapter(adp2);
        corpusSpinner.setPrompt("Корпус");
        if(addressData.corpusList.size()>0)
            corpusSpinner.setVisibility(VISIBLE);
        else
            corpusSpinner.setVisibility(INVISIBLE);
**/
    }
    public void click_next(View view) {
        //дом
        Spinner houseSpinner = (Spinner)findViewById(R.id.house_spinner);
        if(houseSpinner.getSelectedItem() != null)
            myApp.userData.addressData.house = houseSpinner.getSelectedItem().toString();
        //корпус
        EditText editText = (EditText)findViewById(R.id.corpus_value);
        myApp.userData.addressData.corpus = editText.getText().toString();
        //квартира
        editText = (EditText)findViewById(R.id.apartment_value);
        myApp.userData.addressData.appartment = editText.getText().toString();
        new DownloadTask(this).execute(2);
    }

    public void postDownload(DownloadTask.DownloadResult downloadResult){
        switch (downloadResult){
            case OK:{
                Intent intent = new Intent(AddressActivity.this, DataActivity.class);
                startActivity(intent);
            }break;
            case NO_DATA:{
                UIHelper.showMessageBox(this,"Нет связи с сервером.",null);
            }break;
            case NO_PARSE:{
                UIHelper.showMessageBox(this,"Не удалось получить данные.","Проверьте адрес.");
            }break;
        }
    }

    public void click_back(View view) {
        super.onBackPressed();
    }

}
