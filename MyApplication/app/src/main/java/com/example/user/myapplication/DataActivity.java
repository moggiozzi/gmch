package com.example.user.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DataActivity extends AppCompatActivity {
    private MyApplication myApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        myApp = (MyApplication)getApplication();

        UserData userData = myApp.userData;
        TextView textView = (TextView) findViewById(R.id.saldo);
        textView.setText(userData.saldo);

        textView = (TextView) findViewById(R.id.factoryNumber);
        textView.setText(userData.factoryNumber);

        textView = (TextView) findViewById(R.id.lastCounter);
        textView.setText(userData.lastCounter);

        EditText editText = (EditText) findViewById(R.id.currentCounter);
        editText.setText(userData.currentCounter);

        textView = (TextView) findViewById(R.id.consumCounter);
        textView.setText(userData.consumeCounter);

        textView = (TextView) findViewById(R.id.date);
        textView.setText(userData.date);
        // если все ок, то сохраняем данные пользователя
        // если пользователь еще не в списке, то добавим его
        if(myApp.getUserDataById(myApp.userData.userId) == null) {
            myApp.addNewUserData(myApp.userData);
        }
        myApp.saveData();
    }
    public void click_next(View view) {
        EditText editText = (EditText)findViewById(R.id.currentCounter);
        myApp.userData.currentCounter = editText.getText().toString();
        new DownloadTask(this).execute(3);
    }
    public void postDownload(DownloadTask.DownloadResult downloadResult){
        switch (downloadResult){
            case OK:{
                UserData userData = myApp.userData;
                EditText editText = (EditText) findViewById(R.id.currentCounter);
                editText.setText(userData.currentCounter);

                TextView textView = (TextView) findViewById(R.id.consumCounter);
                textView.setText(userData.consumeCounter);

                textView = (TextView) findViewById(R.id.date);
                textView.setText(userData.date);
            }break;
            case NO_DATA:{
                UIHelper.showMessageBox(this,"Нет связи с сервером.",null);
            }break;
            case NO_PARSE:{
                UIHelper.showMessageBox(this,"Не удалось получить данные.","Проверьте адрес.");
            }break;
        }
    }
    public void click_back(View view) {
        super.onBackPressed();
    }
}
