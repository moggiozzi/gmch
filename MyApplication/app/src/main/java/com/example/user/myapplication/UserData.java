package com.example.user.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by User on 21.01.2017.
 */

public class UserData {
    String userId;
    String cookie;
    AddressData addressData;

    String saldo;
    String factoryNumber;
    String lastCounter;
    String currentCounter;
    String consumeCounter;
    String date;
    public UserData(){
        addressData = new AddressData();
        Date date = new Date();
        cookie = Long.toString(date.getTime());
        Random r = new Random();
        while(cookie.length() < 19)
            cookie += Integer.toString(r.nextInt(10));
    }
    public UserData(UserData ud){
        userId = ud.userId;
        cookie = ud.cookie;
        addressData = new AddressData(ud.addressData);

        saldo = ud.saldo;
        factoryNumber = ud.factoryNumber;
        lastCounter = ud.lastCounter;
        currentCounter = ud.currentCounter;
        consumeCounter = ud.consumeCounter;
        date = ud.date;
    }
    public void reset(){
        saldo=factoryNumber=lastCounter=currentCounter=consumeCounter=date="";
        addressData.reset();
    }
}
