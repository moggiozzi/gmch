package com.example.user.myapplication;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by User on 15.01.2017.
 * Класс приложения для хранения глобальных данных
 */

public class MyApplication extends Application {
    private static final int MAX_USER_DATA_SIZE = 5;
    ArrayList<UserData> userDataList = new ArrayList<UserData>();
    UserData userData = null;
    public UserData getUserDataById(String id){
        for(int i=0;i<userDataList.size();i++)
            if(userDataList.get(i).userId.equals(id))
                return userDataList.get(i);
        return null;
    }
    public void addNewUserData(UserData ud){
        if(userDataList.size() < MAX_USER_DATA_SIZE){
            userDataList.add(ud);
        }else{
            // найдем самого "старого" пользователя
            int oldUserIdx = 0;
            for(int i=1;i<userDataList.size();i++){
                if(userDataList.get(oldUserIdx).cookie.compareTo(userDataList.get(i).cookie) > 0)
                    oldUserIdx = i;
            }
            userDataList.set(oldUserIdx,ud);
        }

    }
    public boolean loadData(){
        Context context = getApplicationContext();
        for(int i=0;i<MAX_USER_DATA_SIZE;i++) {
            SharedPreferences sharedPref = context.getSharedPreferences("user" + i, Context.MODE_PRIVATE);
            if(sharedPref.getString("userId","").equals(""))
                continue;
            UserData ud = new UserData();
            ud.userId = sharedPref.getString("userId","");
            ud.cookie = sharedPref.getString("cookie","");

            ud.addressData.house = sharedPref.getString("house","");
            ud.addressData.corpus = sharedPref.getString("corpus","");
            ud.addressData.appartment = sharedPref.getString("appartment","");
            userDataList.add(ud);
            userData = ud;
        }
        return true;
    }
    public boolean saveData(){
        Context context = getApplicationContext();
        for(int i=0;i<Math.min(userDataList.size(),MAX_USER_DATA_SIZE);i++) {
            if(userData == userDataList.get(i)) { // сохранить данные только текущего пользователя
                SharedPreferences sharedPref = context.getSharedPreferences("user" + i, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("userId", userDataList.get(i).userId);
                editor.putString("cookie", userDataList.get(i).cookie);

                editor.putString("house", userDataList.get(i).addressData.house);
                editor.putString("corpus", userDataList.get(i).addressData.corpus);
                editor.putString("appartment", userDataList.get(i).addressData.appartment);

                editor.commit();
                break;
            }
        }
        return true;
    }
    public void clearAllData(){
        Context context = getApplicationContext();
        for(int i=0;i<MAX_USER_DATA_SIZE;i++) {
            SharedPreferences sharedPref = context.getSharedPreferences("user" + i, Context.MODE_PRIVATE);
            if (sharedPref == null)
                continue;
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear();
            editor.commit();
        }
    }
}
